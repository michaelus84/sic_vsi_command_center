#include "parameters.h"
#include "ui_parameters.h"
#include "client.h"
#include <iostream>

ParametersClass::ParametersClass(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parameters)
{
  ui->setupUi(this);

  connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(NewParameters()));
}

ParametersClass::~ParametersClass()
{
  delete ui;
}


/**
 * @brief
 *
 */
void ParametersClass::NewParameters(void)
{
  uint8_t data[10];
  uint16_t index = 0;

  //std::cout<<"Wysylam parametry"<<std::endl;

  data[index++] = CMD_SET_POLE_PAIRS;
  data[index++] = ui->pole_pairs->value();
  emit SendParameters(data, index);

  index = 0;

  data[index++] = CMD_SET_ANGLE_OFFSET;
  UdpClientClass::FloatToByteTable((float)ui->electric_angle_offset->value(), data, &index);

  emit SendParameters(data, index);

  this->accept();
}

void ParametersClass::SetPolePairs(uint8_t pole_pairs)
{
  ui->pole_pairs->setValue(pole_pairs);
}

void ParametersClass::SetAngleOffset(float offset)
{
  ui->electric_angle_offset->setValue(offset);
}

#include "client.h"
#include "math.h"
#include <QList>
#include <iostream>

typedef union
{
  uint32_t u32;
  float f;
} f32;

/**
 * @brief 
 * 
 */
void UdpClientClass::ConfigClient(void)
{
  main_udp_socket = new QUdpSocket(this);

  main_udp_socket->bind(DEFAULT_DATA_PORT, QUdpSocket::ShareAddress);

  connect(main_udp_socket, SIGNAL(readyRead()), this, SLOT(MainData()));

  control_udp_socket = new QUdpSocket(this);
  control_udp_socket->bind(DEFAULT_CONFIG_PORT_LOCAL, QUdpSocket::ShareAddress);

  plots = 0;

  memset(&udp_fifo, 0, sizeof(FifoTypedef));
  memset(&udp_buffer, 0, sizeof(UdpBufferTypedef));
  udp_fifo.len = MAX_UDP_FIFO_LEN;

  connect(control_udp_socket, SIGNAL(readyRead()), this, SLOT(ControlData()));
  connect(&control_timer, SIGNAL(timeout()), this, SLOT(ControlDataTimeout()));
  control_timer.start(10);
}

/**
 * @brief 
 * 
 * @param data 
 * @param ip 
 */
void UdpClientClass::SendUdp(uint8_t * data, int len, QString ip)
{
  if (len > MAX_UDP_DATA_LEN) return;

  //wrzucamy nasze dane do kolejki
  memcpy(udp_buffer[udp_fifo.end].buffer, data, len);
  udp_buffer[udp_fifo.end].len = len;
  udp_buffer[udp_fifo.end].ip = QHostAddress(ip);
  udp_buffer[udp_fifo.end].error_cnt = 0;
  IncrementIndex(&udp_fifo.end, 1, udp_fifo.len);

  //kolejka pusta, inicjujemy wysylanie
  if (!udp_fifo.fill)
  {
    control_udp_socket->writeDatagram((const char *)udp_buffer[udp_fifo.start].buffer,
                                                  udp_buffer[udp_fifo.start].len,
                                                  udp_buffer[udp_fifo.start].ip,
                                                  DEFAULT_CONFIG_PORT_REMOTE);
    control_timer.start(10);
  }
  udp_fifo.fill++;
  timeout = 0;
}

/**
 * @brief 
 * 
 */
void UdpClientClass::MainData(void)
{
  while (main_udp_socket->hasPendingDatagrams())
  {
    frame_datagram.resize(main_udp_socket->pendingDatagramSize());
    main_udp_socket->readDatagram(frame_datagram.data(), frame_datagram.size());

    int j = 0;

    status = 0;

    plots = frame_datagram.at(j++);
    for (int l = 0; l < plots; l++)
    {
      plots_list[l] = frame_datagram.at(j++);
    }
    int nb_index = 0;

    samples = frame_datagram.at(j++);

    for (int i = 0; i < samples; i++)
    {
      for (int w = 0; w < plots + 1; w++)
      {
        inverter_data[nb_index++] = ByteTableToFloat(frame_datagram.data(), &j);
      }
    }
    status = frame_datagram.at(j);
    emit InverterData(inverter_data);
  }
}

/**
 * @brief 
 * 
 * @param tab 
 * @param j 
 * @return float 
 */
float UdpClientClass::ByteTableToFloat(char * tab, int * j)
{
  f32 out;
  int tmp = *j;
  out.u32 = (uint32_t)(*(tab + tmp++) & 0x000000FF);
  out.u32 |= (uint32_t)((*(tab + tmp++) & 0x000000FF)) << 8;
  out.u32 |= (uint32_t)((*(tab + tmp++) & 0x000000FF)) << 16;
  out.u32 |= (uint32_t)((*(tab + tmp++) & 0x000000FF)) << 24;
  *j = tmp;
  return out.f;
}

/**
 * @brief 
 * 
 * @param tab 
 * @param j 
 * @return int 
 */
int UdpClientClass::ByteTableToInt(char * tab, int * j)
{
  unsigned int integer;

  int tmp = *j;
  integer = (unsigned int)(*(tab + tmp) & 0x000000FF);
  tmp++;
  integer |= (unsigned int)((*(tab + tmp) & 0x000000FF)) << 8;
  tmp++;
  integer |= (unsigned int)((*(tab + tmp) & 0x000000FF)) << 16;
  tmp++;
  integer |= (unsigned int)((*(tab + tmp) & 0x000000FF)) << 24;
  tmp++;
  *j = tmp;

  return integer;
}

/**
 * @brief 
 * 
 * @param number 
 * @param data_out 
 * @param index 
 */
void UdpClientClass::FloatToByteTable(float number, uint8_t *data_out, uint16_t *index)
{
  f32 d;
  d.f = number;

  *(data_out + (*index)++) = (uint8_t)(d.u32 & 0x000000FF);
  *(data_out + (*index)++) = (uint8_t)((d.u32 & 0x0000FF00) >> 8);
  *(data_out + (*index)++) = (uint8_t)((d.u32 & 0x00FF0000) >> 16);
  *(data_out + (*index)++) = (uint8_t)((d.u32 & 0xFF000000) >> 24);
}

/**
 * @brief 
 * 
 * @param number 
 * @param data_out 
 * @param index 
 */
void UdpClientClass::ShortUintToByteTable(uint16_t number, uint8_t *data_out, uint16_t *index)
{
  *(data_out + (*index)++) = (uint8_t)(number & 0x000000FF);
  *(data_out + (*index)++) = (uint8_t)((number & 0x0000FF00) >> 8);
}

/**
 * @brief 
 * 
 * @param number 
 * @param data_out 
 * @param index 
 */
void UdpClientClass::LongUintToByteTable(uint32_t number, uint8_t *data_out, uint16_t *index)
{
  *(data_out + (*index)++) = (uint8_t)(number & 0x000000FF);
  *(data_out + (*index)++) = (uint8_t)((number & 0x0000FF00) >> 8);
  *(data_out + (*index)++) = (uint8_t)((number & 0x00FF0000) >> 16);
  *(data_out + (*index)++) = (uint8_t)((number & 0xFF000000) >> 24);
}

/**
 * @brief 
 * 
 */
uint8_t UdpClientClass::ControlDataRead(uint8_t cmd)
{
  uint8_t * data;
  int len;
  int i;
  QString result;

  while (control_udp_socket->hasPendingDatagrams())
  {
    i = 0;
    control_datagram.resize(control_udp_socket->pendingDatagramSize());
    len = control_datagram.size();

    control_udp_socket->readDatagram(control_datagram.data(), control_datagram.size());

    data = (uint8_t *)control_datagram.data();

    do
    {
      // sprawdzamy czy mamy minimalna liczbe danych potrzebnych do analizy
      if (len < 3) return RETURN_BUSY;
      // sprawdzamy czy typ odpowidzi sie zgadza
      if (CMD_RESULT != data[i++]) break;
      // spradzamy czy to co otrzymalismy zgadza sie z komenda ktora wyslismy
      if (cmd != data[i++]) break;
      // sprawdzamy statusy odpowiedzi
      if (CODE_OK == data[i])
      {
        result = "SUCCESS";
      }
      else if (CODE_RESPONSE == data[i])
      {
        emit ResponseExe(cmd, data[i + 1], &data[i + 2]);
        result = "RESPONSE";
      }
      else
      {
        result = "ERROR " + QString::number(cmd);
      }
      emit ControlCmd(result);

      return RETURN_SUCCESS;
    } while (false);

    result = "WRONG RESPONSE";
    emit ControlCmd(result);

    return RETURN_FAILURE;
  }
  return RETURN_BUSY;
}

/**
 * @brief 
 * 
 * @param index_ptr 
 * @param offset 
 * @param range 
 */
void UdpClientClass::IncrementIndex(int * index_ptr, int offset, int range)
{
  int new_index;

  new_index = *index_ptr + offset;
  // sprawdz, czy wynik miesci sie w zakresie wielkosci bufora
  if (new_index >= range)
  {
    new_index %= range;
  }
  // zapisz wynik
  *index_ptr = new_index;
}

/**
 * @brief Funkcja odbierajca komunikaty zwrotne od urzadzenia
 *
 */
void UdpClientClass::ControlData(void)
{
  uint8_t status = ControlDataRead(last_cmd);
  bool send = false;

  if (RETURN_BUSY != status)
  {
    if (++udp_buffer[udp_fifo.start].error_cnt >= MAX_ERROR_CNT)
    {
      // Konczymy na bledzie
      udp_fifo.fill = 0;
      udp_fifo.start = 0;
      udp_fifo.end = 0;
      control_timer.stop();

      emit Restore(last_cmd);
      return;
    }
    else
    {
      send = true;
    }
  }
  else if (RETURN_SUCCESS == status)
  {
    if (udp_fifo.fill)
    {
      IncrementIndex(&udp_fifo.start, 1, udp_fifo.len);
      udp_fifo.fill--;
      send = true;
    }
    else control_timer.stop();
  }

  if (send)
  {
    timeout = 0;

    last_cmd = udp_buffer[udp_fifo.start].buffer[0];
    control_udp_socket->writeDatagram((const char *)udp_buffer[udp_fifo.start].buffer,
                                                    udp_buffer[udp_fifo.start].len,
                                                    udp_buffer[udp_fifo.start].ip,
                                                    DEFAULT_CONFIG_PORT_REMOTE);
  }
}

/**
 * @brief obsluga timeoutu od transmisji
 *
 */
void UdpClientClass::ControlDataTimeout(void)
{
  if (++timeout > 10)
  {
    // Konczymy na bedzie
    udp_fifo.fill = 0;
    udp_fifo.start = 0;
    udp_fifo.end = 0;
    send_status = SEND_IDLE;
    emit ControlCmd(QString("TIMEOUT"));
    emit Restore(last_cmd);
    control_timer.stop();
  }
}

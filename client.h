#ifndef CLIENT_H
#define CLIENT_H
#include <QObject>
#include <QTimer>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QHostAddress>
#include "plot_select.h"

#define DEFAULT_CONFIG_PORT_LOCAL  12000
#define DEFAULT_CONFIG_PORT_REMOTE 12000
#define DEFAULT_DATA_PORT          11000
#define MAX_UDP_DATA_LEN           1500
#define MAX_UDP_FIFO_LEN           200

#define RETURN_SUCCESS            0
#define RETURN_FAILURE            1
#define RETURN_BUSY               2
#define RETURN_IDLE               3

#define MAX_ERROR_CNT             3

typedef enum
{
  CMD_ACTION_RESET = 0,
  CMD_ACTION_START,
  CMD_SET_OPEN_LOOP,
  CMD_SET_REFERENCE,
  CMD_SET_D_REFERENCE,
  CMD_SET_PATTEN_ENABLE,
  CMD_SET_PATTERN_MAP,
  CMD_SET_LOAD_ENABLE,
  CMD_SET_LOAD_MAP,
  CMD_SET_RAMP,
  CMD_SET_PLOT_LIST,
  CMD_SET_GENERAL_PURPOSE_PLOT,
  CMD_SET_COEFFICIENTS,
  CMD_SET_FREQUENCY,
  CMD_SET_ANGLE_OFFSET,
  CMD_GET_ANGLE_OFFSET,
  CMD_SET_POLE_PAIRS,
  CMD_GET_POLE_PAIRS,
  CMD_SET_PWM_EN,
  MAX_CMD,
  CMD_RESULT,
} CommandEnumTypedef;

enum
{
  SEND_IDLE = 0,
  SEND_WRITE,
  SEND_RESPONSE,
};

enum
{
  CODE_OK = 0,
  CODE_ERROR,
  CODE_RESPONSE,
};

typedef struct
{
  int start;
  int end;
  int len;
  int fill;
} FifoTypedef;

typedef struct 
{
  uint8_t buffer[MAX_UDP_DATA_LEN];
  int len;
  QHostAddress ip;
  int error_cnt;
} UdpBufferTypedef;


class UdpClientClass : public QObject
{
    Q_OBJECT
    
public:
  QUdpSocket * main_udp_socket;
  QHostAddress main_host;
  QUdpSocket* control_udp_socket;
  QHostAddress control_host;
  QTcpSocket * tcpsocket;
  QByteArray frame_datagram;
  QByteArray control_datagram;
  unsigned char status;
  int plots_list[MAX_GRAPHS - 1];
  int plots;
  int config_index;
  int samples;

  void ConfigClient(void);
  float ByteTableToFloat(char * tab, int * j);
  int ByteTableToInt(char * tab, int * j);
  static void FloatToByteTable(float number, uint8_t * data_out, uint16_t * index);
  void ShortUintToByteTable(uint16_t number, uint8_t * data_out, uint16_t * index);
  void LongUintToByteTable(uint32_t number, uint8_t * data_out, uint16_t * index);
  void SendUdp(uint8_t * data, int len, QString ip);

private:
  uint8_t ControlDataRead(uint8_t cmd);
  FifoTypedef udp_fifo;
  UdpBufferTypedef udp_buffer[MAX_UDP_FIFO_LEN];
  QTimer control_timer;
  uint8_t send_status;
  void IncrementIndex(int * index_ptr, int offset, int range);
  float inverter_data[500];
  uint8_t last_cmd;
  uint32_t timeout;


public slots:
  void MainData(void);



private slots:
    void ControlData(void);
    void ControlDataTimeout(void);

signals:
  void InverterData(float* data);
  void ControlCmd(QString data);
  void ConfigStage(int index);
  void Restore(int index);
  void ResponseExe(uint8_t cmd, uint16_t len, uint8_t * data);

};

#endif // CLIENT_H

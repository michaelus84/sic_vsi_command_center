#include "plot_select.h"
#include "ui_plot_select.h"
#include <iostream>
#include "client.h"
#include <string.h>
#include <stdint.h>

// kazdy z wykresow moze miec kilka podwykresow, tutaj mamy mape ktora to mowi ktory podwykres
// numerowany bezwzglednie od 0 do MAX_CUSTOM_GRAPHS, przypada ktoremu wykresowi
const unsigned char plot_table[MAX_GRAPHS] = {0, 0, 0, 0, 0, 1, 1, 2, 3, 4, 4, 4, 5, 5, 6, 6, 6, 7, 8, 8, 8, 8, 8, 8, 9};

/**
 * @brief Construct a new Plot Select Class:: Plot Select Class object
 * 
 * @param parent 
 */
PlotSelectClass::PlotSelectClass(QWidget *parent) : QDialog(parent),
                                            ui(new Ui::plot_select)
{
  int i = 0;

  ui->setupUi(this);

  plot[i++] = ui->id;
  plot[i++] = ui->iq;
  plot[i++] = ui->id_pattern;

  plot[i++] = ui->iqs;
  plot[i++] = ui->ids;

  plot[i++] = ui->ud;
  plot[i++] = ui->uq;

  plot[i++] = ui->velocity;
  plot[i++] = ui->angle;

  plot[i++] = ui->ia;
  plot[i++] = ui->ib;
  plot[i++] = ui->ic;

  plot[i++] = ui->ias;
  plot[i++] = ui->ibs;

  plot[i++] = ui->ua;
  plot[i++] = ui->ub;
  plot[i++] = ui->uc;

  plot[i++] = ui->udc;

  plot[i++] = ui->upd;
  plot[i++] = ui->upddown;
  plot[i++] = ui->updup;

  plot[i++] = ui->upq;
  plot[i++] = ui->upqdown;
  plot[i++] = ui->upqup;
  plot[i++] = ui->exe_time;

  connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(NewPlotList()));
}

/**
 * @brief Destroy the Plot Select Class:: Plot Select objecty
 * 
 */
PlotSelectClass::~PlotSelectClass()
{
  delete ui;
}

/**
 * @brief 
 * 
 */
void PlotSelectClass::NewPlotList(void)
{
  unsigned char data[MAX_GRAPHS + 2];

  int j = 0;
  int i;
  int index = 0;

  // zerujemy tablice aktywnych wykresow
  for (i = 0; i < MAX_PLOTS; i++) active_plot[i] = false;

  // Pierwsze miejsce na liscie domyslenie przeznaczone jest na wzorzec
  plot_list[j++] = PATTERN_GRAPH;

  // Tworzymy liste aktywnych procesow
  // przeszukiwanie po wszytkich podwykresach
  for (i = 0; i < MAX_GRAPHS - 1; i++)
  {
    if (plot[i]->checkState() == Qt::Checked)
    {
      active_plot[plot_table[i]] = true;
      plot_list[j++] = i + 1;
    }
  }

  data[index++] = CMD_SET_PLOT_LIST;
  data[index++] = j;

  memcpy(&data[index], plot_list, j);
  index += j;

  this->accept();

  emit SendPlotList(data, index);
}

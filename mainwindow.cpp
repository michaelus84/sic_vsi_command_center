﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "client.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <QFileDialog>
#include <stdint.h>
#include <cmath>


int const frequency_scheme[MAX_FREQUENCY]   = {5, 10, 15, 20, 22, 25, 33, 40, 48}; // frequency
std::string mode_scheme[MODE_MAX]           = {"o", "p", "w", "i", "u"}; // mode
QString const mode_text[MODE_MAX]           = {"open loop","servo", "velocity", "iq", "uq"}; // tekst dla trybu pracy

char const *plot_name[]                     = {"id.", "iq.", "isq.", "isd.", "ud.", "uq.", "w.", "angle.", "ia.", "ib.", "ic.", "ia_lc.", "ib_lc.", "ua.", "ub.", "uc.", "udc.", "upd.", "upd_down.", "upd_up.", "upq.", "upq_down.", "upq_up.", "gp."};

QString const empty[]                       = {"empty"};
QString const servo_sfc[]                   = {"servo_sfc", "Kp:", "Kt:", "Psi_f:", "Ls:", "vel_n:", "I_n:", "Uster_n:", "tau_w:", "Adm:", "Bdm:", "tau_i:", "Ade:", "Bde:", "obs1:", "obs2:", "obs3:", "obs4:", "obs5:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:", "Kp_v:", "Ki_v:", "Kawu_v:", "Kp_p:", "Ki_p:", "Kawu_p:", "K_SFC_1_1:", "K_SFC_1_2:", "K_SFC_1_3:", "K_SFC_1_4:", "K_SFC_1_5:", "K_SFC_2_1:", "K_SFC_2_2:", "K_SFC_2_3:", "K_SFC_2_4:", "K_SFC_2_5:", "K_Tl_d:", "K_Tl_q:", "K_awu_sfc:"};
QString const servo_ccs[]                   = {"servo_ccs", "Ls:", "Psi_f:", "vel_n:", "I_n:", "Uster_n:", "obs_1:", "obs_2:", "obs_3:", "obs_4:", "obs_5:", "Kp_p:", "Ki_p:", "Kawu_p:", "Kp_v:", "Ki_v:", "Kawu_v:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:"};
QString const velocity_non_linear[]         = {"velocity_non_linear", "Ls:", "Psi_f:", "vel_n:", "I_n:", "Uster_n:", "ae:", "be:", "ce:", "ad1:", "bd1:", "cd1:", "ad2:", "bd2:", "ad3:", "bd3:", "ad4:", "bd4:", "aq1:", "bq1:", "aq2:", "bq2:", "cq2:", "aq3:", "bq3:", "cq3:", "aq4:", "bq4:", "cq4:", "Kawu_sfc:"};
QString const velocity_sfc_with_lc_filter[] = {"velocity_sfc_with_lc_filter", "Lf:", "Ls:", "Psi_f:", "vel_n:", "I_n:", "Uster_n:", "Kawu_vel:", "ae:", "am:", "be:", "bm:", "ce:", "cm:", "K_1_1:", "K_1_2:", "K_1_3:", "K_1_4:", "K_1_5:", "K_1_6:", "K_1_7:", "K_1_8:", "K_1_9:", "K_2_1:", "K_2_2:", "K_2_3:", "K_2_4:", "K_2_5:", "K_2_6:", "K_2_7:", "K_2_8:", "K_2_9:"};
QString const current_pi[]                  = {"current_pi", "Ls:", "Psi_f:", "I_n:", "Uster_n:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:"};
QString const servo_sfc_imc[]               = {"servo_sfc_imc", "Kp:", "Kt:", "Psi_f:", "Ls:", "vel_n:", "I_n:", "Uster_n:", "tau_w:", "Adm:", "Bdm:", "obs1:", "obs2:", "obs3:", "obs4:", "obs5:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:", "K_SFC_1:", "K_SFC_2:", "K_SFC_3:", "K_SFC_4:", "K_awu_sfc:"};
QString const velocity_ccs[]                = {"velocity_ccs", "Ls:", "Psi_f:", "I_n:", "Uster_n:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:", "Kp_w:", "Ki_w:", "Kawu_w:", "Tfw:"};
QString const ucq_with_lc_filter[]          = {"ucq_with_lc_filter", "Kp:", "Lf:", "Cf:", "I_n:", "Uster_n:", "a:", "b:", "an:", "Kawu:", "obs1:", "obs2:", "obs3:", "obs4:", "obs5:", "obs6:", "obs7:", "obs8:", "obs9:", "obs10:", "K_LC_1:", "K_LC_2:", "K_LC_3:", "K_LC_4:", "K_LC_5:", "K_LC_6:", "K_LC_7:", "K_LC_8:", "K_LC_9:", "K_LC_10:", "K_LC_11:", "K_LC_12:", "K_ff_1:", "K_ff_2:", "K_ff_3:", "K_ff_4:", "K_ff_5:", "K_ff_6:", "K_ff_7:", "K_ff_8:"};
//QString const servo_sfc_imc_ff[]          = {"servo_sfc_imc_ff", "Kp:", "Kt:", "Psi_f:", "Ls:", "vel_n:", "I_n:", "Uster_n:", "tau_w:", "Adm:",  "Bdm:", "obs1:", "obs2:", "obs3:", "obs4:", "obs5:", "Kp_id:", "Ki_id:", "Kawu_id:", "Kp_iq:", "Ki_iq:", "Kawu_iq:", "K_awu_sfc:", "K_SFC_1:", "K_SFC_2:", "K_SFC_3:", "K_ff:", "K_awu_sfc:"};
QString const velocity_sfc[]                = {"velocity_sfc", "Kp:", "Psi_f:", "Ls:", "I_n:", "Uster_n:", "tau_i:", "Ade:", "Bde:", "obs1:", "obs2:", "obs3:", "obs4:", "obs5:","K_SFC_1_1:", "K_SFC_1_2:", "K_SFC_1_3:", "K_SFC_1_4:", "K_SFC_1_5:", "K_SFC_2_1:", "K_SFC_2_2:", "K_SFC_2_3:", "K_SFC_2_4:", "K_SFC_2_5:", "K_ff_1_1:", "K_ff_1_2:", "K_ff_2_1:", "K_ff_2_2:", "K_awu_sfc:" };

/**
 * @brief Construct a new Main Window:: Main Window object
 * 
 * @param parent 
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  plotselect = new PlotSelectClass;
  parameters = new ParametersClass;

  int i;
  int j;

  controllers_scheme[OPEN_LOOP_CONTROL].str = empty;
  controllers_scheme[OPEN_LOOP_CONTROL].items = _NumItems(empty);

  controllers_scheme[VELOCITY_CCS].str = velocity_ccs;
  controllers_scheme[VELOCITY_CCS].items = _NumItems(velocity_ccs) - 1;

  controllers_scheme[SERVO_SFC].str = servo_sfc;
  controllers_scheme[SERVO_SFC].items = _NumItems(servo_sfc) - 1;

  controllers_scheme[SERVO_CCS].str = servo_ccs;
  controllers_scheme[SERVO_CCS].items = _NumItems(servo_ccs) - 1;

  controllers_scheme[VELOCITY_NON_LINEAR].str = velocity_non_linear;
  controllers_scheme[VELOCITY_NON_LINEAR].items = _NumItems(velocity_non_linear) - 1;

  controllers_scheme[VELOCITY_SFC_WITH_LC_FILTER].str = velocity_sfc_with_lc_filter;
  controllers_scheme[VELOCITY_SFC_WITH_LC_FILTER].items = _NumItems(velocity_sfc_with_lc_filter) - 1;

  controllers_scheme[UCQ_WITH_LC_FILTER].str = ucq_with_lc_filter;
  controllers_scheme[UCQ_WITH_LC_FILTER].items = _NumItems(ucq_with_lc_filter) - 1;

  controllers_scheme[CURRENT_PI].str = current_pi;
  controllers_scheme[CURRENT_PI].items = _NumItems(current_pi) - 1;

  controllers_scheme[SERVO_SFC_IMC].str = servo_sfc_imc;
  controllers_scheme[SERVO_SFC_IMC].items = _NumItems(servo_sfc_imc) - 1;

  controllers_scheme[VELOCITY_SFC].str = velocity_sfc;
  controllers_scheme[VELOCITY_SFC].items = _NumItems(velocity_sfc) - 1;


  connect(ui->actionPlot_select, SIGNAL(triggered()), plotselect, SLOT(exec()));
  connect(ui->actionParameters, SIGNAL(triggered()), this, SLOT(PrametersOpen()));

  pause_recieve = false;
  total_time = 0;
  data_len = 0;
  udp_client = new UdpClientClass;

  i = 0;
  j = 0;

  SetupGrid(ui->d_q_values, j);
  SetupPlot(i++, j);
  SetupPlot(i++, j++);
  SetupGrid(ui->angle_speed, j);
  SetupPlot(i++, j);
  SetupPlot(i++, j++);
  SetupGrid(ui->phase_currents, j);
  SetupPlot(i++, j);
  SetupPlot(i++, j++);
  SetupGrid(ui->voltage, j);
  SetupPlot(i++, j);
  SetupPlot(i++, j++);
  SetupGrid(ui->reg_signals, j);
  SetupPlot(i++, j);

  QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
  gpp_select = new QComboBox();
  gpp_select->setSizePolicy(sizePolicy1);
  gpp_select->setMaximumSize(QSize(90, 20));
  gpp_select->insertItems(0, QStringList()
                                 << QApplication::translate("MainWindow", "exe time", 0)
                                 << QApplication::translate("MainWindow", "ele. ag.", 0)
                                 << QApplication::translate("MainWindow", "load ref", 0)
                                 << QApplication::translate("MainWindow", "isdobs", 0)
                                 << QApplication::translate("MainWindow", "uCdobs", 0)
                                 << QApplication::translate("MainWindow", "isqobs", 0)
                                 << QApplication::translate("MainWindow", "uCqobs", 0));

  SetupGpPlot(i, j, gpp_select);

  i = 0;

  pattern_time[i++] = ui->time1;
  pattern_time[i++] = ui->time2;
  pattern_time[i++] = ui->time3;
  pattern_time[i++] = ui->time4;
  pattern_time[i++] = ui->time5;
  pattern_time[i++] = ui->time6;
  pattern_time[i++] = ui->time7;
  pattern_time[i++] = ui->time8;
  pattern_time[i++] = ui->time9;
  pattern_time[i++] = ui->time10;
  pattern_time[i++] = ui->time11;

  i = 0;

  pattern_value[i++] = ui->value1;
  pattern_value[i++] = ui->value2;
  pattern_value[i++] = ui->value3;
  pattern_value[i++] = ui->value4;
  pattern_value[i++] = ui->value5;
  pattern_value[i++] = ui->value6;
  pattern_value[i++] = ui->value7;
  pattern_value[i++] = ui->value8;
  pattern_value[i++] = ui->value9;
  pattern_value[i++] = ui->value10;
  pattern_value[i++] = ui->value11;

  i = 0;

  d_pattern_value[i++] = ui->d_val_1;
  d_pattern_value[i++] = ui->d_val_2;
  d_pattern_value[i++] = ui->d_val_3;
  d_pattern_value[i++] = ui->d_val_4;
  d_pattern_value[i++] = ui->d_val_5;
  d_pattern_value[i++] = ui->d_val_6;
  d_pattern_value[i++] = ui->d_val_7;
  d_pattern_value[i++] = ui->d_val_8;
  d_pattern_value[i++] = ui->d_val_9;
  d_pattern_value[i++] = ui->d_val_10;
  d_pattern_value[i++] = ui->d_val_11;

  i = 0;

  load_time[i++] = ui->time1_2;
  load_time[i++] = ui->time2_2;
  load_time[i++] = ui->time3_2;
  load_time[i++] = ui->time4_2;
  load_time[i++] = ui->time5_2;
  load_time[i++] = ui->time6_2;
  load_time[i++] = ui->time7_2;
  load_time[i++] = ui->time8_2;
  load_time[i++] = ui->time9_2;
  load_time[i++] = ui->time10_2;
  load_time[i++] = ui->time11_2;

  i = 0;

  load_value[i++] = ui->value1_2;
  load_value[i++] = ui->value2_2;
  load_value[i++] = ui->value3_2;
  load_value[i++] = ui->value4_2;
  load_value[i++] = ui->value5_2;
  load_value[i++] = ui->value6_2;
  load_value[i++] = ui->value7_2;
  load_value[i++] = ui->value8_2;
  load_value[i++] = ui->value9_2;
  load_value[i++] = ui->value10_2;
  load_value[i++] = ui->value11_2;

  for (i = 0; i < 10; i++)
  {
    plot[i]->addGraph();
    plot[i]->axisRect()->setupFullAxesBox(true);
    plot[i]->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    plot[i]->xAxis->setRange(0, 100, Qt::AlignCenter);
    plot[i]->yAxis->setRange(0, 10, Qt::AlignCenter);

    x_scale[i]->setValue(100);
    y_scale[i]->setValue(10);

    QString text;
    text = QString::number(100);
    x_scale_edit[i]->setText(text);
    text = QString::number(10);
    y_scale_edit[i]->setText(text);

    plot[i]->xAxis->setLabel("t[ms]");
    xscale[i] = 100;
  }

  AdditionalGraphs();

  QFont legendFont = font();
  legendFont.setPointSize(9);

  for (i = 0; i < 10; i++)
  {
    plot[i]->legend->setFont(legendFont);
    plot[i]->legend->setBrush(QBrush(QColor(255, 255, 255, 230)));
    plot[i]->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom | Qt::AlignRight);
    plot[i]->legend->setVisible(true);
  }

  i = 0;
  //------------- d q values ---------------
  plot[i++]->yAxis->setLabel("id, iq[A]");

  plot[i++]->yAxis->setLabel("ud, uq[V]");

  //------------- mechanical ---------------
  plot[i++]->yAxis->setLabel("speed[rad/s]");
  plot[i++]->yAxis->setLabel("angle[rad]");

  //------------- phase current ------------
  plot[i++]->yAxis->setLabel("i befor LC [A]");
  plot[i++]->yAxis->setLabel("i behind LC [A]");

  //------------- voltage ------------------
  plot[i++]->yAxis->setLabel("phase u[V]");
  plot[i++]->yAxis->setLabel("udc[V]");

  plot[i++]->yAxis->setLabel("regulator signals [V/V]");

  plot[i++]->yAxis->setLabel("time [us]");
  connect(gpp_select, SIGNAL(currentIndexChanged(int)), this, SLOT(GeneralPurposePlot(int)));

  connect(ui->sendpattern, SIGNAL(clicked()), this, SLOT(NewReferencePattern()));
  connect(ui->load_en, SIGNAL(stateChanged(int)), this, SLOT(LoadEnable(int)));
  connect(ui->ramp, SIGNAL(currentIndexChanged(int)), this, SLOT(RampEnable(int)));
  connect(ui->pattern_en, SIGNAL(stateChanged(int)), this, SLOT(PatternEnable(int)));

  connect(ui->stop_receive, SIGNAL(clicked()), this, SLOT(StopReceive()));
  connect(ui->start_receive, SIGNAL(clicked()), this, SLOT(StartReceive()));

  udp_client->ConfigClient();
  connect(udp_client, SIGNAL(InverterData(float *)), this, SLOT(Plot(float *)));
  connect(udp_client, SIGNAL(ControlCmd(QString)), ui->status_line_edit, SLOT(setText(QString)));
  connect(ui->start_button, SIGNAL(clicked()), this, SLOT(InverterStart()));
  connect(ui->stop_button, SIGNAL(clicked()), this, SLOT(InverterStop()));
  connect(ui->save, SIGNAL(clicked()), this, SLOT(SaveData()));

  i = 0;

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_0(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_0(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_1(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_1(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_2(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_2(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_3(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_3(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_4(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_4(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_5(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_5(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_6(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_6(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_7(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_7(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_8(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_8(int)));

  connect(x_scale[i], SIGNAL(valueChanged(int)), this, SLOT(x_scale_change_9(int)));
  connect(y_scale[i++], SIGNAL(valueChanged(int)), this, SLOT(y_scale_change_9(int)));

  i = 0;

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_0()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_0()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_1()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_1()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_2()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_2()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_3()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_3()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_4()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_4()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_5()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_5()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_6()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_6()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_7()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_7()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_8()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_8()));

  connect(x_scale_edit[i], SIGNAL(editingFinished()), this, SLOT(x_scale_edited_9()));
  connect(y_scale_edit[i++], SIGNAL(editingFinished()), this, SLOT(y_scale_edited_9()));

  connect(ui->reference, SIGNAL(valueChanged(int)), this, SLOT(NewReferenceValue(int)));
  connect(ui->reference_line_edit, SIGNAL(editingFinished()), this, SLOT(NewTextReferenceValue()));

  connect(ui->aux_ref, SIGNAL(valueChanged(int)), this, SLOT(NewAuxReferenceValue(int)));
  connect(ui->reference_line_edit_2, SIGNAL(editingFinished()), this, SLOT(NewAuxTextReferenceValue()));

  connect(ui->open_loop, SIGNAL(stateChanged(int)), this, SLOT(OpenLoopSet(int)));

  SetupQLed();

  connect(ui->clear, SIGNAL(clicked()), this, SLOT(ClearGraphs()));

  connect(ui->plotselect, SIGNAL(clicked()), plotselect, SLOT(open()));
  connect(ui->coefficientset, SIGNAL(clicked()), this, SLOT(CoefficientOpen()));
  connect(plotselect, SIGNAL(SendPlotList(unsigned char *, uint16_t)), this, SLOT(SendUdpData(unsigned char *, uint16_t)));
  connect(ui->reset, SIGNAL(clicked()), this, SLOT(Restart()));
  connect(ui->frequency, SIGNAL(currentIndexChanged(int)), this, SLOT(SwitchingFrequency(int)));
  connect(ui->sendpattern_2, SIGNAL(clicked()), this, SLOT(NewLoadPattern()));

  connect(udp_client, SIGNAL(Restore(int)), this, SLOT(Restore(int)));

  connect(parameters, SIGNAL(SendParameters(unsigned char *, uint16_t)), this, SLOT(SendUdpData(unsigned char *, uint16_t)));

  connect(ui->pwm_en, SIGNAL(stateChanged(int)), this, SLOT(PwmEnable(int)));

  connect(ui->polar, SIGNAL(stateChanged(int)), this, SLOT(PolarValue(int)));

  connect(udp_client, SIGNAL(ResponseExe(uint8_t, uint16_t, uint8_t *)), this, SLOT(ResponseExe(uint8_t, uint16_t, uint8_t *)));

  ui->dest_ip->setText("192.168.0.10");
  OpenLoopSet(Qt::Checked);
  plotselect->NewPlotList();
}

/**
 * @brief
 *
 */
int MainWindow::PrametersOpen(void)
{
  uint8_t data[2];

  data[0] = CMD_GET_ANGLE_OFFSET;
  data[1] = 0;
  SendUdpData(data, 2);

  data[0] = CMD_GET_POLE_PAIRS;
  data[1] = 0;
  SendUdpData(data, 2);

  return parameters->exec();
}


void MainWindow::ResponseExe(uint8_t cmd, uint16_t len, uint8_t * data)
{
  float f;
  int index = 0;

  switch(cmd)
  {
    case CMD_GET_ANGLE_OFFSET:
      if (4 == len)
      {
        f = udp_client->ByteTableToFloat((char *)data, &index);
        parameters->SetAngleOffset(f);
      }
      break;

    case CMD_GET_POLE_PAIRS:
      if (1 == len) parameters->SetPolePairs(data[0]);
      break;
  }
}

/**
 * @brief Destroy the Main Window:: Main Window object
 * 
 */
MainWindow::~MainWindow()
{
  delete ui;
}

/**
 * @brief
 *
 */
void MainWindow::Restore(int index)
{
  if (CMD_SET_OPEN_LOOP == index)
  {
    OpenLoopRestore();
  }
}

/**
 * @brief 
 * 
 */
void MainWindow::CoefficientOpen(void)
{
  int i;
  int j;
  QString line;

  ui->status_line_edit->clear();

  static bool new_path = false;
  static QString init_path;

  if (!new_path)
  {
    init_path = qApp->applicationDirPath();
    new_path = true;
  }

  QString filename = QFileDialog::getOpenFileName(this, "Open coefficients file", init_path, "*.coef *.txt");
  init_path = filename;

  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;

  QTextStream coefficient_file(&file);
  QString type = coefficient_file.readLine();
  type.replace("  ", " ");
  QList<QString> list;
  list = type.split(' ');

  j = 0;

  // Odczyt typu regulatora
  QString controller_str = list[j++];

  for (i = 0; i < MAX_CONTROLLERS; i++)
  {
    if (controller_str == controllers_scheme[i].str[0])
    {
      config_data.ct_len = controllers_scheme[i].items;
      config_data.controller = i;
      ui->con_type->setText(controller_str);
      break;
    }
  }

  if (MAX_CONTROLLERS == i) return;

  // odczyt czestotliwosci na ktorej ma pracowac
  int freq = list[j++].toInt();

  for (i = 0; i < MAX_FREQUENCY; i++)
  {
    if (freq == frequency_scheme[i])
    {
      config_data.frequency = i;
      if (ui->frequency->currentIndex() != i)
        ui->frequency->setCurrentIndex(i);
      else SwitchingFrequency(i);
      break;
    }
  }

  if (MAX_FREQUENCY == i) return;

  // Odczyt trybu pracy
  std::string mode_string = list[j++].toStdString();

  for (i = 0; i < MODE_MAX; i++)
  {
    if (mode_scheme[i] == mode_string)
    {
      config_data.mode = i;
      const QSignalBlocker blocker(ui->open_loop);
      ui->open_loop->setCheckState(Qt::Unchecked);
      ControllerMode(i);
      ui->mode->setText(mode_text[i]);
      break;
    }
  }

  if (MODE_MAX == i) return;

  // Odczyt wspolczynnikow dla regulatora
  for (i = 0; i < config_data.ct_len; i++)
  {
    // Odczytujemy linie zawierajaca dane dla regulatora
    line = coefficient_file.readLine();
    line.replace(",", ".");
    line.replace("  ", " ");
    line.replace(" \n", "\n");

    list = line.split(' ');
    // przeszukujemy w celu znaleziania pasujacej wartosci do schematu
    for (j = 0; j < config_data.ct_len; j++)
    {
      if (list[0] == controllers_scheme[config_data.controller].str[j + 1]) break;
    }
    config_data.coef[j] = list[1].toFloat();
  }

  // Odczyt wzorca dla referncji
  j = 0;
  line = coefficient_file.readLine();

  if (line.contains("pattern"))
  {
    while (!coefficient_file.atEnd())
    {
      if (j > PATTERN_LEN) break;

      line = coefficient_file.readLine();

      if (line.contains("load")) break;

      line.replace(",", ".");
      line.replace("  ", " ");
      line.replace(" \n", "\n");
      list = line.split(' ');
      pattern_time[j]->setValue(list[0].toInt());
      pattern_value[j]->setValue(list[1].toFloat());
      j++;
    }
  }

  // Odczyt wzorca dla obciazenia
  j = 0;

  if (line.contains("load"))
  {
    while (!coefficient_file.atEnd())
    {
      if (j > PATTERN_LEN) break;

      line = coefficient_file.readLine();
      if (line.isEmpty()) break;
      line.replace(",", ".");
      line.replace("  ", " ");
      line.replace(" \n", "\n");
      list = line.split(' ');
      load_time[j]->setValue(list[0].toInt());
      load_value[j]->setValue(list[1].toFloat());
      j++;
    }
  }
  config_data.new_file = true;
  file.close();

  SendControllerData(config_data.coef, config_data.controller, config_data.ct_len);
  NewReferencePattern();
  plotselect->NewPlotList();
}

/**
 * @brief 
 * 
 * @param index 
 */
void MainWindow::GeneralPurposePlot(int index)
{
  uint8_t data[2];

  switch (index)
  {
    case EXECUTION_TIME:
      plot[9]->yAxis->setLabel("time [us]");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case ELECTRIC_ANGLE:
      plot[9]->yAxis->setLabel("rad");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case SSI_T_OBS_OLD:
      plot[9]->yAxis->setLabel("load [Nm/Nm]");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case LC_P_ISDOBS:
      plot[9]->yAxis->setLabel("isdobs");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case LC_P_UCDOBS:
      plot[9]->yAxis->setLabel("uCdobs");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case LC_P_ISQOBS:
      plot[9]->yAxis->setLabel("isqobs");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
    case LC_P_UCQOBS:
      plot[9]->yAxis->setLabel("uCqobs");
      plot[9]->replot(QCustomPlot::rpImmediate);
      break;
  }

  data[0] = CMD_SET_GENERAL_PURPOSE_PLOT;
  data[1] = index;
  SendUdpData(data, 2);
}

/**
 * @brief 
 * 
 * @param freq_index 
 */
void MainWindow::SwitchingFrequency(int freq_index)
{
  uint8_t data[2];

  if (freq_index < MAX_FREQUENCY)
  {
    data[0] = CMD_SET_FREQUENCY;
    data[1] = freq_index;

    SendUdpData(data, 2);
  }
}


/**
 * @brief 
 * 
 */
void MainWindow::InverterStart(void)
{
  InverterEnable(true);
}

/**
 * @brief 
 * 
 */
void MainWindow::InverterStop(void)
{
  InverterEnable(false);
}

/**
 * @brief 
 * 
 * @param start 
 */
void MainWindow::InverterEnable(bool start)
{
  uint8_t data[2];

  if (start) ClearGraphs();

  pause_recieve = !start;

  ui->frequency->setDisabled(start);
  ui->con_type->setDisabled(start);
  ui->load_en->setDisabled(start);
  ui->ramp->setDisabled(start);
  ui->pattern_en->setDisabled(start);
  ui->sendpattern->setDisabled(start);
  ui->sendpattern_2->setDisabled(start);
  ui->time1->setDisabled(start);
  ui->time2->setDisabled(start);
  ui->time3->setDisabled(start);
  ui->time4->setDisabled(start);
  ui->time5->setDisabled(start);
  ui->time6->setDisabled(start);
  ui->time7->setDisabled(start);
  ui->time8->setDisabled(start);
  ui->time9->setDisabled(start);
  ui->time10->setDisabled(start);
  ui->time11->setDisabled(start);

  ui->time1_2->setDisabled(start);
  ui->time2_2->setDisabled(start);
  ui->time3_2->setDisabled(start);
  ui->time4_2->setDisabled(start);
  ui->time5_2->setDisabled(start);
  ui->time6_2->setDisabled(start);
  ui->time7_2->setDisabled(start);
  ui->time8_2->setDisabled(start);
  ui->time9_2->setDisabled(start);
  ui->time10_2->setDisabled(start);
  ui->time11_2->setDisabled(start);

  ui->value1->setDisabled(start);
  ui->value2->setDisabled(start);
  ui->value3->setDisabled(start);
  ui->value4->setDisabled(start);
  ui->value5->setDisabled(start);
  ui->value6->setDisabled(start);
  ui->value7->setDisabled(start);
  ui->value8->setDisabled(start);
  ui->value9->setDisabled(start);
  ui->value10->setDisabled(start);
  ui->value11->setDisabled(start);

  ui->value1_2->setDisabled(start);
  ui->value2_2->setDisabled(start);
  ui->value3_2->setDisabled(start);
  ui->value4_2->setDisabled(start);
  ui->value5_2->setDisabled(start);
  ui->value6_2->setDisabled(start);
  ui->value7_2->setDisabled(start);
  ui->value8_2->setDisabled(start);
  ui->value9_2->setDisabled(start);
  ui->value10_2->setDisabled(start);
  ui->value11_2->setDisabled(start);

  ui->d_val_1->setDisabled(start);
  ui->d_val_2->setDisabled(start);
  ui->d_val_3->setDisabled(start);
  ui->d_val_4->setDisabled(start);
  ui->d_val_5->setDisabled(start);
  ui->d_val_6->setDisabled(start);
  ui->d_val_7->setDisabled(start);
  ui->d_val_8->setDisabled(start);
  ui->d_val_9->setDisabled(start);
  ui->d_val_10->setDisabled(start);
  ui->d_val_11->setDisabled(start);

  ui->coefficientset->setDisabled(start);
  ui->start_button->setDisabled(start);
  ui->clear->setDisabled(start);
  ui->stop_button->setDisabled(!start);

  ui->mode->setDisabled(start);

  data[0] = CMD_ACTION_START;
  data[1] = start;
  SendUdpData(data, 2);
}

/**
 * @brief 
 * 
 * @param index 
 */
void MainWindow::RampEnable(int index)
{
  uint8_t data[2];

  data[0] = CMD_SET_RAMP;

  if (index <= PATTERN_S_CURVE)
  {
    data[1] = index;
    SendUdpData(data, 2);
  }
}

/**
 * @brief 
 * Senf
 * @param index 
 */
void MainWindow::LoadEnable(int index)
{
  uint8_t data[2];

  data[0] = CMD_SET_LOAD_ENABLE;

  if (index == Qt::Checked) data[1] = 1;
  else if (index == Qt::Unchecked) data[1] = 0;
  else return;

  SendUdpData(data, 2);
}

/**
 * @brief 
 * 
 * @param index 
 */
void MainWindow::PatternEnable(int index)
{
  uint8_t data[2];
  uint32_t time;

  data[0] = CMD_SET_PATTEN_ENABLE;

  if (index == Qt::Checked)
  {
    data[1] = 1;

    for (int i = 0; i < PATTERN_LEN; i++)
    {
      time = pattern_time[i]->value();
      if (time > total_time) total_time = time;
    }
  }
  else if (index == Qt::Unchecked)
  {
    data[1] = 0;
    total_time = 0;
  }
  else return;

  SendUdpData(data, 2);
}

/**
 * @brief 
 * 
 */
void MainWindow::NewReferencePattern(void)
{
  float value;
  uint32_t time;
  uint16_t j;
  unsigned char * reference_data = new unsigned char[4 * PATTERN_LEN * sizeof(uint32_t)];

  j = 0;

  reference_data[j++] = CMD_SET_PATTERN_MAP;
  reference_data[j++] = config_data.mode;

  reference_data[j++] = PATTERN_LEN;

  switch (config_data.mode)
  {
    case MODE_SERVO:
      graph_ptr[0] = plot[3]->graph(1);
      break;

    case MODE_VELOCITY:
      graph_ptr[0] = plot[2]->graph(1);
      break;

    case MODE_I:
      graph_ptr[0] = plot[0]->graph(2);
      break;

    case MODE_U:
      graph_ptr[0] = plot[1]->graph(2);
      break;
  }

  total_time = 0;

  for (int i = 0; i < PATTERN_LEN; i++)
  {
    time = pattern_time[i]->value();
    value = pattern_value[i]->value();
    udp_client->LongUintToByteTable(time, reference_data, &j);
    udp_client->FloatToByteTable(value, reference_data, &j);
    value = d_pattern_value[i]->value();
    udp_client->FloatToByteTable(value, reference_data, &j);

    if (time > total_time) total_time = time;
  }

  total_time += 50; //ms

  SendUdpData(reference_data, j);

  delete[] reference_data;
}

/**
 * @brief 
 * 
 */
void MainWindow::NewLoadPattern(void)
{
  uint32_t value;
  uint32_t time;
  uint16_t j;
  uint16_t i;

  uint8_t data[PATTERN_LEN * sizeof(uint32_t) + 2];

  j = 0;

  data[j++] = CMD_SET_LOAD_MAP;
  data[j++] = PATTERN_LEN;

  if (UCQ_WITH_LC_FILTER == config_data.controller)
  {
    for (i = 0; i < PATTERN_LEN; i++)
    {
      time = load_time[i]->value();
      value = load_value[i]->value();
      udp_client->LongUintToByteTable(time, data, &j);
      udp_client->LongUintToByteTable(value, data, &j);
    }
  }
  else
  {
    for (i = 0; i < PATTERN_LEN; i++)
    {
      time = load_time[i]->value();
      value = (double)load_value[i]->value() * EXTERNAL_LOAD_SCALE + EXTERNAL_LOAD_OFFSET;
      udp_client->LongUintToByteTable(time, data, &j);
      udp_client->LongUintToByteTable(value, data, &j);
    }
  }

  SendUdpData(data, j);
}

/**
 * @brief 
 * 
 * @param i 
 */
void MainWindow::NewPatternSlot(int i)
{
  switch (i)
  {
    case MODE_SERVO:
      ui->value_label->setText("Value [rad]:");
      break;

    case MODE_VELOCITY:
      ui->value_label->setText("Value [rad/s]:");
      break;

    case MODE_I:
      ui->value_label->setText("Value [A]:");
      break;

    case MODE_U:
      ui->value_label->setText("Value [V]:");
      break;
  }
}

/**
 * @brief 
 * 
 */
void MainWindow::StopReceive(void)
{
  pause_recieve = true;
}

/**
 * @brief 
 * 
 */
void MainWindow::StartReceive(void)
{
  pause_recieve = false;
}

/**
 * @brief 
 * 
 */
void MainWindow::ClearGraphs(void)
{
  int i;
  pause_recieve = true;

  for (i = 0; i < MAX_PLOTS; i++)
  {
    plot[i]->clearGraphs();
    plot[i]->addGraph();
    plot[i]->axisRect()->setupFullAxesBox(true);
    plot[i]->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    plot[i]->xAxis->setRange(0, 100, Qt::AlignCenter);
    plot[i]->yAxis->setRange(0, 10, Qt::AlignCenter);
    x_scale[i]->setValue(100);
    y_scale[i]->setValue(10);
  }

  AdditionalGraphs();

  switch (config_data.mode)
  {
    case MODE_SERVO:
      graph_ptr[0] = plot[3]->graph(1);
      break;
    case MODE_VELOCITY:
      graph_ptr[0] = plot[2]->graph(1);
      break;
    case MODE_I:
      graph_ptr[0] = plot[0]->graph(2);
      break;
    case MODE_U:
      graph_ptr[0] = plot[1]->graph(2);
      break;
  }

  for (i = 0; i < MAX_PLOTS; i++)
  {
    plot[i]->replot(QCustomPlot::rpImmediate);
  }

  data_len = 0;

  ui->reference->setValue(0);
  status_rdy->setValue(false);
  status_desat->setValue(false);
  status_error->setValue(false);
  status_breaking->setValue(false);
}

/**
 * @brief 
 * 
 */
void MainWindow::AdditionalGraphs(void)
{
  int i = 0;
  //------------- id graph -----------------
  plot[i]->graph(0)->setName("id");
  //------------- iq graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(1)->setName("iq");
  plot[i]->addGraph();
  plot[i]->graph(2)->setPen(QPen(Qt::red));
  plot[i]->graph(2)->setName("iq pattern");

  plot[i]->addGraph();
  plot[i]->graph(3)->setPen(QPen(Qt::green));
  plot[i]->graph(3)->setName("iqs");

  plot[i]->addGraph();
  plot[i]->graph(4)->setPen(QPen(Qt::yellow));
  plot[i]->graph(4)->setName("ids");

  plot[i]->addGraph();
  plot[i]->graph(5)->setPen(QPen(Qt::darkRed));
  plot[i]->graph(5)->setName("id pattern");

  i++;

  //------------- ud graph -----------------
  plot[i]->graph(0)->setName("ud");
  //------------- uq graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(1)->setName("uq");
  //------------- uq pattern -----------------
  plot[i]->addGraph();
  plot[i]->graph(2)->setPen(QPen(Qt::red));
  plot[i]->graph(2)->setName("uq pattern");

  i++;

  plot[i]->graph(0)->setName("v");
  //------------ speed pattern -------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::red));
  plot[i]->graph(1)->setName("v pattern");

  i++;

  plot[i]->graph(0)->setName("position");
  //------------ positio_npattern -----------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::red));
  plot[i]->graph(1)->setName("pos. pattern");

  i++;

  plot[i]->graph(0)->setName("ia");
  //------------- ib graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(1)->setName("ib");
  //------------- ic graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(2)->setPen(QPen(Qt::green));
  plot[i]->graph(2)->setName("ic");

  i++;

  plot[i]->graph(0)->setName("ias");
  //------------- ib_lc graph --------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(1)->setName("ibs");

  i++;

  plot[i]->graph(0)->setName("ua");
  //------------- ub graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(1)->setName("ub");
  //------------- uc graph -----------------
  plot[i]->addGraph();
  plot[i]->graph(2)->setPen(QPen(Qt::green));
  plot[i]->graph(2)->setName("uc");
  //------------- u pattern -----------------
  //plot[i]->addGraph();
  //plot[i]->graph(3)->setPen(QPen(Qt::red));

  i++;

  plot[i]->graph(0)->setName("Udc");

  i++;

  //------------- upd down --------------
  plot[i]->graph(0)->setName("upd");
  //------------- upd down --------------
  plot[i]->addGraph();
  plot[i]->graph(1)->setPen(QPen(Qt::magenta));
  plot[i]->graph(1)->setName("upd down");
  //------------- upd up ----------------
  plot[i]->addGraph();
  plot[i]->graph(2)->setPen(QPen(Qt::green));
  plot[i]->graph(2)->setName("upd up");
  //------------- upq -------------------
  plot[i]->addGraph();
  plot[i]->graph(3)->setPen(QPen(Qt::darkYellow));
  plot[i]->graph(3)->setName("upq");
  //------------- upq down---------------
  plot[i]->addGraph();
  plot[i]->graph(4)->setPen(QPen(Qt::darkMagenta));
  plot[i]->graph(4)->setName("upq down");
  //------------- upq up-----------------
  plot[i]->addGraph();
  plot[i]->graph(5)->setPen(QPen(Qt::darkGreen));
  plot[i]->graph(5)->setName("upq up");

  i++;

  plot[i]->graph(0)->setName("data");

  i = 0;

  // create pointers to graphs //
  graph_ptr[i++] = plot[2]->graph(1);

  graph_ptr[i++] = plot[0]->graph(0);
  graph_ptr[i++] = plot[0]->graph(1);

  graph_ptr[i++] = plot[0]->graph(5);

  graph_ptr[i++] = plot[0]->graph(3);
  graph_ptr[i++] = plot[0]->graph(4);

  graph_ptr[i++] = plot[1]->graph(0);
  graph_ptr[i++] = plot[1]->graph(1);

  graph_ptr[i++] = plot[2]->graph(0);
  graph_ptr[i++] = plot[3]->graph(0);

  graph_ptr[i++] = plot[4]->graph(0);
  graph_ptr[i++] = plot[4]->graph(1);
  graph_ptr[i++] = plot[4]->graph(2);

  graph_ptr[i++] = plot[5]->graph(0);
  graph_ptr[i++] = plot[5]->graph(1);

  graph_ptr[i++] = plot[6]->graph(0);
  graph_ptr[i++] = plot[6]->graph(1);
  graph_ptr[i++] = plot[6]->graph(2);

  graph_ptr[i++] = plot[7]->graph(0);

  graph_ptr[i++] = plot[8]->graph(0);
  graph_ptr[i++] = plot[8]->graph(1);
  graph_ptr[i++] = plot[8]->graph(2);
  graph_ptr[i++] = plot[8]->graph(3);
  graph_ptr[i++] = plot[8]->graph(4);
  graph_ptr[i++] = plot[8]->graph(5);

  graph_ptr[i++] = plot[9]->graph(0);
}

/**
 * @brief 
 * 
 */
void MainWindow::Restart(void)
{
  uint8_t data[1];

  data[0] = CMD_ACTION_RESET;
  SendUdpData(data, 1);

  ui->reference->setValue(0);
  ui->status_line_edit->clear();
  data_len = 0;
  total_time = 0;
}

/**
 * @brief 
 * 
 * @param value 
 */
void MainWindow::NewReferenceValue(int value)
{
  uint8_t data[sizeof(float) + 2];
  float reference = 0.0f;
  uint16_t j = 0;
  uint8_t mode;

  QString text;

  if (Qt::Checked == ui->open_loop->checkState()) mode = MODE_OPEN_LOOP;
  else mode = config_data.mode;

  data[j++] = CMD_SET_REFERENCE;
  data[j++] = mode;

  switch (mode)
  {
    case MODE_OPEN_LOOP:
    case MODE_SERVO:
      reference = (float)value / 100.0f;
      text = QString::number(reference);
      ui->reference_line_edit->setText(text);
      break;

    case MODE_VELOCITY:
      reference = 2.0f * M_PI * (float)value / 60.0f;
      text = QString::number(reference);
      ui->reference_line_edit->setText(text);
      text += " rad/s";
      break;

    case MODE_I:
      reference = (float)value / 1000.0f;
      text = QString::number(reference);
      ui->reference_line_edit->setText(text);
      break;

    case MODE_U:
      reference = (float)value / 10.0f;
      text = QString::number(reference);
      ui->reference_line_edit->setText(text);
      break;
  }

  if (Qt::Checked == ui->polar->checkState())
  {
    reference = reference * std::sin(0.0174532925f * ui->aux_ref->value());
  }

  udp_client->FloatToByteTable(reference, data, &j);

  SendUdpData(data, j);

  if (Qt::Checked == ui->polar->checkState())
  {
    j = 0;
    data[j++] = CMD_SET_D_REFERENCE;
    data[j++] = mode;
    reference = reference * std::cos(0.0174532925f * ui->aux_ref->value());
    udp_client->FloatToByteTable(reference, data, &j);
    SendUdpData(data, j);
  }
}

/**
 * @brief
 *
 * @param value
 */
void MainWindow::NewAuxReferenceValue(int value)
{
  uint8_t data[sizeof(float) + 2];
  float reference = 0.0f;
  float xs = 0.0f;
  uint16_t j = 0;

  uint8_t mode;

  QString text;

  if (Qt::Checked == ui->open_loop->checkState()) mode = MODE_OPEN_LOOP;
  else mode = config_data.mode;

  data[j++] = CMD_SET_D_REFERENCE;
  data[j++] = mode;

  if (Qt::Unchecked == ui->polar->checkState())
  {
    switch (mode)
    {
      case MODE_OPEN_LOOP:
        reference = (float)value / 100.0f;
        text = QString::number(reference);
        ui->reference_line_edit_2->setText(text);
        break;

      case MODE_I:
        reference = (float)value / 1000.0f;
        text = QString::number(reference);
        ui->reference_line_edit_2->setText(text);
        break;
      default:
        return;
        break;
    }
  }
  else
  {
    switch (mode)
    {
      case MODE_OPEN_LOOP:
        xs = (float)ui->reference->value()/ 100.0f;
        break;

      case MODE_I:
        xs = (float)ui->reference->value()/ 1000.0f;
        break;
    }
    text = QString::number(value);
    ui->reference_line_edit_2->setText(text);

    reference = xs * std::cos(0.0174532925f * value);
  }

  udp_client->FloatToByteTable(reference, data, &j);
  SendUdpData(data, j);

  if (Qt::Checked == ui->polar->checkState())
  {
    j = 0;
    data[j++] = CMD_SET_REFERENCE;
    data[j++] = mode;
    reference = xs * std::sin(0.0174532925f * value);
    udp_client->FloatToByteTable(reference, data, &j);
    SendUdpData(data, j);
  }
}

/**
 * @brief 
 * 
 */
void MainWindow::NewTextReferenceValue(void)
{
  double tmp;

  uint8_t mode;

  if (Qt::Checked == ui->open_loop->checkState()) mode = MODE_OPEN_LOOP;
  else mode = config_data.mode;

  tmp = ui->reference_line_edit->text().toDouble();

  switch (mode)
  {
    case MODE_OPEN_LOOP:
    case MODE_SERVO:
      ui->reference->setValue(tmp * 100);
      break;

    case MODE_VELOCITY:
      ui->reference->setValue(tmp * 1000);
      break;

    case MODE_I:
      ui->reference->setValue(tmp);
      break;

    case MODE_U:
      ui->reference->setValue(tmp * 10);
      break;
  }
}

/**
 * @brief
 *
 */
void MainWindow::NewAuxTextReferenceValue(void)
{
  double tmp;

  uint8_t mode;

  if (Qt::Checked == ui->open_loop->checkState()) mode = MODE_OPEN_LOOP;
  else mode = config_data.mode;

  tmp = ui->reference_line_edit_2->text().toDouble();

  if (Qt::Checked == ui->polar->checkState())
  {
    ui->aux_ref->setValue(tmp);
  }
  else
  {
    switch (mode)
    {
      case MODE_OPEN_LOOP:
        ui->aux_ref->setValue(tmp * 100);
        break;

      case MODE_I:
        ui->aux_ref->setValue(tmp);
        break;
    }
  }
}

/**
 * @brief
 *
 */
void MainWindow::PolarValue(int status)
{
  uint8_t mode;

  if (Qt::Checked == ui->open_loop->checkState()) mode = MODE_OPEN_LOOP;
  else mode = config_data.mode;

  if (Qt::Checked == status)
  {
    switch (mode)
    {
      case MODE_OPEN_LOOP:
        ui->reference->setMaximum(100);
        ui->reference->setMinimum(-100);
        ui->unit_label->setText("");
        break;
      case MODE_I:
        ui->reference->setMaximum(5000);
        ui->reference->setMinimum(-5000);
        ui->unit_label->setText("A");
        break;

      default:
        if (Qt::Checked == status)
        {
          const QSignalBlocker blocker(ui->polar);
          ui->polar->setCheckState(Qt::Unchecked);
        }
        return;
        break;
    }
    ui->aux_ref->setDisabled(false);
    ui->aux_ref->setMaximum(360);
    ui->aux_ref->setMinimum(-360);
    ui->unit_label_aux->setText("deg");
  }
  else
  {
    ControllerMode(mode);
  }
}

/**
 * @brief
 *
 */
void MainWindow::OpenLoopSet(int status)
{
  uint8_t data[2];

  data[0] = CMD_SET_OPEN_LOOP;

  if (Qt::Checked == status)
  {
    ControllerMode(MODE_OPEN_LOOP);
    data[1] = 1;
  }
  else
  {
    ControllerMode(config_data.mode);
    data[1] = 0;
  }
  SendUdpData(data, 2);
}

/**
 * @brief
 *
 */
void MainWindow::PwmEnable(int status)
{
  uint8_t data[2];

  data[0] = CMD_SET_PWM_EN;

  if (status == Qt::Checked) data[1] = 1;
  else if (status == Qt::Unchecked) data[1] = 0;
  else return;

  SendUdpData(data, 2);
}

/**
 * @brief
 *
 */
void MainWindow::OpenLoopRestore(void)
{
  const QSignalBlocker blocker(ui->open_loop);

  if (Qt::Checked == ui->open_loop->checkState())
    ui->open_loop->setCheckState(Qt::Unchecked);
  else if (Qt::Unchecked == ui->open_loop->checkState())
    ui->open_loop->setCheckState(Qt::Checked);
}

/**
 * @brief 
 * 
 * @param index 
 */
void MainWindow::ControllerMode(int index)
{
  switch (index)
  {
    case MODE_OPEN_LOOP:
      ui->reference->setMaximum(100);
      ui->reference->setMinimum(-100);
      ui->aux_ref->setDisabled(false);
      ui->aux_ref->setMaximum(100);
      ui->aux_ref->setMinimum(-100);
      ui->unit_label->setText("");
      ui->unit_label_aux->setText("");
      break;

    case MODE_SERVO:
      ui->reference->setMaximum(800);
      ui->reference->setMinimum(-800);
      ui->unit_label->setText("rad");
      ui->aux_ref->setDisabled(true);
      ui->unit_label_aux->setText("");
      break;

    case MODE_VELOCITY:
      ui->reference->setMaximum(2400);
      ui->reference->setMinimum(-2400);
      ui->unit_label->setText("rad/s");
      ui->aux_ref->setDisabled(true);
      ui->unit_label_aux->setText("");
      break;

    case MODE_I:
      ui->reference->setMaximum(5000);
      ui->reference->setMinimum(-5000);
      ui->aux_ref->setDisabled(false);
      ui->aux_ref->setMaximum(5000);
      ui->aux_ref->setMinimum(-5000);
      ui->unit_label->setText("A");
      ui->unit_label_aux->setText("A");
      break;

    case MODE_U:
      ui->reference->setMaximum(3000);
      ui->reference->setMinimum(-3000);
      ui->unit_label->setText("V");
      ui->aux_ref->setDisabled(true);
      ui->unit_label_aux->setText("");
      break;
  }
}

/**
 * @brief 
 * 
 */
void MainWindow::SaveData(void)
{
  static bool new_path = false;
  static QString init_path = NULL;
  QString name;
  std::ofstream out;
  const QCPDataMap *dataMap;
  QMap<double, QCPData>::const_iterator i;
  QString filename;

  if (!new_path)
  {
    init_path = qApp->applicationDirPath();
    if (init_path != NULL) new_path = false;
    else new_path = true;
  }

  if (init_path != NULL)
  {
    filename = QFileDialog::getSaveFileName(this, "Save document...", init_path, "*.txt");
    init_path = filename;

    for (int p = 1; p < udp_client->plots; p++)
    {
      name = filename + "_" + plot_name[udp_client->plots_list[p] - 1] + "txt";
      out.open(name.toStdString().c_str());

      if (out.is_open())
      {
        dataMap = graph_ptr[udp_client->plots_list[p]]->data();
        i = dataMap->constBegin();
        while (i != dataMap->constEnd())
        {
          out << i.value().key << "\t" << i.value().value << std::endl;
          ++i;
        }
        out.close();
      }
    }

    switch (config_data.mode)
    {
      case MODE_SERVO:
        name = filename + '_' + "p_pattern.txt";
        out.open(name.toStdString().c_str());

        if (out.is_open())
        {
          dataMap = plot[3]->graph(1)->data();
          i = dataMap->constBegin();

          while (i != dataMap->constEnd())
          {
            out << i.value().key << "\t" << i.value().value << std::endl;
            ++i;
          }
        }
        out.close();
        break;

      case MODE_VELOCITY:
        name = filename + '_' + "v_pattern.txt";
        out.open(name.toStdString().c_str());

        if (out.is_open())
        {
          dataMap = plot[2]->graph(1)->data();
          i = dataMap->constBegin();

          while (i != dataMap->constEnd())
          {
            out << i.value().key << "\t" << i.value().value << std::endl;
            ++i;
          }
          out.close();
        }
        break;

      case MODE_I:
        name = filename + '_' + "iq_pattern.txt";
        out.open(name.toStdString().c_str());

        if (out.is_open())
        {
          dataMap = plot[0]->graph(2)->data();
          i = dataMap->constBegin();

          while (i != dataMap->constEnd())
          {
            out << i.value().key << "\t" << i.value().value << std::endl;
            i++;
          }
          out.close();
        }
        break;

      case MODE_U:
        name = filename + '_' + "uq_pattern.txt";
        out.open(name.toStdString().c_str());

        if (out.is_open())
        {
          dataMap = plot[1]->graph(2)->data();
          i = dataMap->constBegin();

          while (i != dataMap->constEnd())
          {
            out << i.value().key << "\t" << i.value().value << std::endl;
            ++i;
          }
          out.close();
        }
        break;
    }
  }
}

/**
 * @brief 
 * 
 * @param scale 
 */
void MainWindow::x_scale_change_0(int scale)
{

  XScaleChange(scale, 0);
}

void MainWindow::y_scale_change_0(int scale)
{

  YScaleChange(scale, 0);
}

void MainWindow::x_scale_change_1(int scale)
{

  XScaleChange(scale, 1);
}

void MainWindow::y_scale_change_1(int scale)
{

  YScaleChange(scale, 1);
}

void MainWindow::x_scale_change_2(int scale)
{

  XScaleChange(scale, 2);
}

void MainWindow::y_scale_change_2(int scale)
{

  YScaleChange(scale, 2);
}

void MainWindow::x_scale_change_3(int scale)
{

  XScaleChange(scale, 3);
}

void MainWindow::y_scale_change_3(int scale)
{

  YScaleChange(scale, 3);
}

void MainWindow::x_scale_change_4(int scale)
{

  XScaleChange(scale, 4);
}

void MainWindow::y_scale_change_4(int scale)
{

  YScaleChange(scale, 4);
}

void MainWindow::x_scale_change_5(int scale)
{

  XScaleChange(scale, 5);
}

void MainWindow::y_scale_change_5(int scale)
{

  YScaleChange(scale, 5);
}

void MainWindow::x_scale_change_6(int scale)
{

  XScaleChange(scale, 6);
}

void MainWindow::y_scale_change_6(int scale)
{

  YScaleChange(scale, 6);
}

void MainWindow::x_scale_change_7(int scale)
{

  XScaleChange(scale, 7);
}

void MainWindow::y_scale_change_7(int scale)
{

  YScaleChange(scale, 7);
}

void MainWindow::x_scale_change_8(int scale)
{

  XScaleChange(scale, 8);
}

void MainWindow::y_scale_change_8(int scale)
{

  YScaleChange(scale, 8);
}

void MainWindow::x_scale_change_9(int scale)
{

  XScaleChange(scale, 9);
}

void MainWindow::y_scale_change_9(int scale)
{

  YScaleChange(scale, 9);
}

/**
 * @brief 
 * 
 * @param scale 
 * @param i 
 */
void MainWindow::YScaleChange(int scale, int i)
{

  plot[i]->yAxis->setRange(0, scale, Qt::AlignCenter);
  QString text;
  text = QString::number(scale);
  y_scale_edit[i]->setText(text);
  plot[i]->replot(QCustomPlot::rpImmediate);
}

/**
 * @brief 
 * 
 * @param scale 
 * @param i 
 */
void MainWindow::XScaleChange(int scale, int i)
{

  xscale[i] = scale;
  plot[i]->xAxis->setRange(data_len, scale, Qt::AlignLeft);
  QString text;
  text = QString::number(scale);
  x_scale_edit[i]->setText(text);
  plot[i]->replot(QCustomPlot::rpImmediate);
}

/**
 * @brief 
 * 
 */
void MainWindow::x_scale_edited_0(void)
{
  XScalEdited(0);
}

void MainWindow::y_scale_edited_0(void)
{
  YScaleEdited(0);
}

void MainWindow::x_scale_edited_1(void)
{
  XScalEdited(1);
}

void MainWindow::y_scale_edited_1(void)
{
  YScaleEdited(1);
}

void MainWindow::x_scale_edited_2(void)
{
  XScalEdited(2);
}

void MainWindow::y_scale_edited_2(void)
{
  YScaleEdited(2);
}

void MainWindow::x_scale_edited_3(void)
{
  XScalEdited(3);
}

void MainWindow::y_scale_edited_3(void)
{
  YScaleEdited(3);
}

void MainWindow::x_scale_edited_4(void)
{
  XScalEdited(4);
}

void MainWindow::y_scale_edited_4(void)
{
  YScaleEdited(4);
}

void MainWindow::x_scale_edited_5(void)
{
  XScalEdited(5);
}

void MainWindow::y_scale_edited_5(void)
{
  YScaleEdited(5);
}

void MainWindow::x_scale_edited_6(void)
{
  XScalEdited(6);
}

void MainWindow::y_scale_edited_6(void)
{
  YScaleEdited(6);
}

void MainWindow::x_scale_edited_7(void)
{
  XScalEdited(7);
}

void MainWindow::y_scale_edited_7(void)
{
  YScaleEdited(7);
}

void MainWindow::x_scale_edited_8(void)
{
  XScalEdited(8);
}

void MainWindow::y_scale_edited_8(void)
{
  YScaleEdited(8);
}

void MainWindow::x_scale_edited_9(void)
{
  XScalEdited(9);
}

void MainWindow::y_scale_edited_9(void)
{
  YScaleEdited(9);
}

/**
 * @brief 
 * 
 * @param i 
 */
void MainWindow::XScalEdited(int i)
{
  int scale = 0;

  scale = x_scale_edit[i]->text().toInt();
  xscale[i] = scale;
  x_scale[i]->setValue(scale);
}

/**
 * @brief 
 * 
 * @param i 
 */
void MainWindow::YScaleEdited(int i)
{
  int scale = 0;
  scale = y_scale_edit[i]->text().toInt();
  y_scale[i]->setValue(scale);
}

/**
 * @brief 
 * 
 * @param data_in 
 */
void MainWindow::Plot(float *data_in)
{

  static int j = 0;

  if (!pause_recieve)
  {
    QCPData g_data;

    for (int k = 0; k < udp_client->samples; k++)
    {
      g_data.key = *(data_in++);

      for (int l = 0; l < udp_client->plots; l++)
      {
        g_data.value = *(data_in++);
        graph_ptr[udp_client->plots_list[l]]->addData(g_data);
      }
    }
    data_len = g_data.key;

    // Wyswietlanie statusu
    if ((udp_client->status & STATUS_RDY))
      status_rdy->setValue(true);
    else
      status_rdy->setValue(false);

    if ((udp_client->status & STATUS_DESAT))
      status_desat->setValue(true);
    else
      status_desat->setValue(false);

    if ((udp_client->status & STATUS_ERROR))
      status_error->setValue(true);
    else
      status_error->setValue(false);

    if ((udp_client->status & STATUS_BREAKING))
      status_breaking->setValue(true);
    else
      status_breaking->setValue(false);

    //-----------------------------------------------------------------------------------------
    if (++j == REFRESH_PLOT_INTERVAL)
    {
      j = 0;

      switch (ui->tabWidget->currentIndex())
      {
        case TAB_1:
          if (plotselect->active_plot[0])
          {
            plot[0]->xAxis->setRange(data_len, xscale[0], Qt::AlignCenter);
            plot[0]->replot(QCustomPlot::rpImmediate);
          }
          if (plotselect->active_plot[1])
          {
            plot[1]->xAxis->setRange(data_len, xscale[1], Qt::AlignCenter);
            plot[1]->replot(QCustomPlot::rpImmediate);
          }
          break;

        case TAB_2:
          if (plotselect->active_plot[2])
          {
            plot[2]->xAxis->setRange(data_len, xscale[2], Qt::AlignCenter);
            plot[2]->replot(QCustomPlot::rpImmediate);
          }
          if (plotselect->active_plot[3])
          {
            plot[3]->xAxis->setRange(data_len, xscale[3], Qt::AlignCenter);
            plot[3]->replot(QCustomPlot::rpImmediate);
          }
          break;

        case TAB_3:
          if (plotselect->active_plot[4])
          {
            plot[4]->xAxis->setRange(data_len, xscale[4], Qt::AlignCenter);
            plot[4]->replot(QCustomPlot::rpImmediate);
          }
          if (plotselect->active_plot[5])
          {
            plot[5]->xAxis->setRange(data_len, xscale[5], Qt::AlignCenter);
            plot[5]->replot(QCustomPlot::rpImmediate);
          }
          break;

        case TAB_4:
          if (plotselect->active_plot[6])
          {
            plot[6]->xAxis->setRange(data_len, xscale[6], Qt::AlignCenter);
            plot[6]->replot(QCustomPlot::rpImmediate);
          }
          if (plotselect->active_plot[7])
          {
            plot[7]->xAxis->setRange(data_len, xscale[7], Qt::AlignCenter);
            plot[7]->replot(QCustomPlot::rpImmediate);
          }
          break;

        case TAB_5:
          if (plotselect->active_plot[8])
          {
            plot[8]->xAxis->setRange(data_len, xscale[8], Qt::AlignCenter);
            plot[8]->replot(QCustomPlot::rpImmediate);
          }
          if (plotselect->active_plot[9])
          {
            plot[9]->xAxis->setRange(data_len, xscale[9], Qt::AlignCenter);
            plot[9]->replot(QCustomPlot::rpImmediate);
          }
          break;
      }
    }
    //-----------------------------------------------------------------------------------------
    if (Qt::Checked == ui->pattern_en->checkState())
    {
      if (total_time > REFRESH_PLOT_INTERVAL)
      {
        if (data_len > total_time)
        {
          pause_recieve = true;
          data_len = 0;
          if (Qt::Checked == ui->autostop->checkState()) InverterStop();
        }
      }
    }
  }
}

/**
 * @brief 
 * 
 * @param event 
 */
void MainWindow::resizeEvent(QResizeEvent *event)
{
  event->accept();
}

/**
 * @brief 
 * 
 * @param parent 
 * @param i 
 */
void MainWindow::SetupGrid(QWidget *parent, int i)
{
  graph_grid[i] = new QGridLayout(parent);
  graph_grid[i]->setSpacing(6);
  graph_grid[i]->setContentsMargins(11, 11, 11, 11);
  graph_grid[i]->setObjectName(QString("chart_grid"));
  graph_grid[i]->setContentsMargins(0, 0, 0, 0);
  graph_grid[i]->setSizeConstraint(QLayout::SetMinimumSize);
}

/**
 * @brief 
 * 
 * @param i 
 * @param j 
 */
void MainWindow::SetupPlot(int i, int j)
{

  plot[i] = new QCustomPlot();
  plot[i]->setObjectName(QString("plot"));
  QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  sizePolicy.setHeightForWidth(plot[i]->sizePolicy().hasHeightForWidth());
  plot[i]->setSizePolicy(sizePolicy);

  graph_grid[j]->addWidget(plot[i], (0x01 & i) + 1, 0, 1, 1);

  controls_grid[i] = new QGridLayout();
  controls_grid[i]->setSpacing(0);
  controls_grid[i]->setObjectName(QString("gridLayout"));

  x_scale[i] = new QDial();
  x_scale[i]->setObjectName(QString("x_scale"));
  QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
  sizePolicy1.setHorizontalStretch(0);
  sizePolicy1.setVerticalStretch(0);
  sizePolicy1.setHeightForWidth(x_scale[i]->sizePolicy().hasHeightForWidth());
  x_scale[i]->setSizePolicy(sizePolicy1);
  x_scale[i]->setMaximum(10000);

  controls_grid[i]->addWidget(x_scale[i], 2, 1, 1, 1);

  y_scale[i] = new QDial();
  y_scale[i]->setObjectName(QString("y_scale"));
  sizePolicy1.setHeightForWidth(y_scale[i]->sizePolicy().hasHeightForWidth());
  y_scale[i]->setSizePolicy(sizePolicy1);
  y_scale[i]->setMaximum(600);

  controls_grid[i]->addWidget(y_scale[i], 0, 1, 1, 1);

  x_scales_label[i] = new QLabel();
  x_scales_label[i]->setObjectName(QString("x_scales_label"));
  sizePolicy1.setHeightForWidth(x_scales_label[i]->sizePolicy().hasHeightForWidth());
  x_scales_label[i]->setSizePolicy(sizePolicy1);
  x_scales_label[i]->setAlignment(Qt::AlignCenter);
  x_scales_label[i]->setMargin(5);

  controls_grid[i]->addWidget(x_scales_label[i], 2, 0, 1, 1);

  y_scales_label[i] = new QLabel();
  y_scales_label[i]->setObjectName(QString("y_scales_label"));
  sizePolicy1.setHeightForWidth(y_scales_label[i]->sizePolicy().hasHeightForWidth());
  y_scales_label[i]->setSizePolicy(sizePolicy1);
  y_scales_label[i]->setAlignment(Qt::AlignCenter);
  y_scales_label[i]->setMargin(5);

  controls_grid[i]->addWidget(y_scales_label[i], 0, 0, 1, 1);

  y_scale_edit[i] = new QLineEdit();
  y_scale_edit[i]->setObjectName(QString("yscale_edit"));
  QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
  sizePolicy2.setHorizontalStretch(0);
  sizePolicy2.setVerticalStretch(0);
  sizePolicy2.setHeightForWidth(y_scale_edit[i]->sizePolicy().hasHeightForWidth());
  y_scale_edit[i]->setSizePolicy(sizePolicy2);
  y_scale_edit[i]->setMaximumSize(QSize(90, 20));
  QFont font;
  font.setPointSize(8);
  y_scale_edit[i]->setFont(font);
  y_scale_edit[i]->setMaxLength(10);
  y_scale_edit[i]->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);

  controls_grid[i]->addWidget(y_scale_edit[i], 1, 1, 1, 1);

  x_scale_edit[i] = new QLineEdit();
  x_scale_edit[i]->setObjectName(QString("xscale_edit"));
  sizePolicy2.setHeightForWidth(x_scale_edit[i]->sizePolicy().hasHeightForWidth());
  x_scale_edit[i]->setSizePolicy(sizePolicy2);
  x_scale_edit[i]->setMaximumSize(QSize(90, 20));
  x_scale_edit[i]->setFont(font);
  x_scale_edit[i]->setMaxLength(10);
  x_scale_edit[i]->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);

  controls_grid[i]->addWidget(x_scale_edit[i], 3, 1, 1, 1);

  graph_grid[j]->addLayout(controls_grid[i], (0x01 & i) + 1, 1, 1, 1);

  x_scales_label[i]->setText(QApplication::translate("MainWindow", "x scale:", 0));
  y_scales_label[i]->setText(QApplication::translate("MainWindow", "y scale:", 0));
}

/**
 * @brief 
 * 
 */
void MainWindow::SetupQLed(void)
{
  QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

  sizePolicy.setHeightForWidth(true);

  status_rdy_label = new QLabel(ui->centralWidget);
  status_rdy_label->setObjectName(QString::fromUtf8("label"));
  status_rdy_label->setContextMenuPolicy(Qt::NoContextMenu);
  status_rdy_label->setAutoFillBackground(false);
  status_rdy_label->setAlignment(Qt::AlignCenter);
  status_rdy_label->setText("RDY:");

  ui->error_layout->addWidget(status_rdy_label);

  status_rdy = new QLed(ui->centralWidget);
  status_rdy->setObjectName(QString::fromUtf8("qLed"));
  status_rdy->setValue(false);
  status_rdy->setOnColor(QLed::Green);
  status_rdy->setOffColor(QLed::Red);
  status_rdy->setShape(QLed::Circle);
  status_rdy->setSizePolicy(sizePolicy);
  status_rdy->setMinimumHeight(30);
  status_rdy->setMinimumWidth(30);

  ui->error_layout->addWidget(status_rdy);

  status_desat_labe = new QLabel(ui->centralWidget);
  status_desat_labe->setObjectName(QString::fromUtf8("label"));
  status_desat_labe->setContextMenuPolicy(Qt::NoContextMenu);
  status_desat_labe->setAutoFillBackground(false);
  status_desat_labe->setAlignment(Qt::AlignCenter);
  status_desat_labe->setText("DESAT:");

  ui->error_layout->addWidget(status_desat_labe);

  status_desat = new QLed(ui->centralWidget);
  status_desat->setObjectName(QString::fromUtf8("qLed"));
  status_desat->setValue(false);
  status_desat->setOnColor(QLed::Green);
  status_desat->setOffColor(QLed::Red);
  status_desat->setShape(QLed::Circle);
  status_desat->setSizePolicy(sizePolicy);
  status_desat->setMinimumHeight(30);
  status_desat->setMinimumWidth(30);

  ui->error_layout->addWidget(status_desat);

  /*
  s3_label = new QLabel(ui->centralWidget);
  s3_label->setObjectName(QString::fromUtf8("label"));
  s3_label->setContextMenuPolicy(Qt::NoContextMenu);
  s3_label->setAutoFillBackground(false);
  s3_label->setAlignment(Qt::AlignCenter);
  s3_label->setText("status_3:");

  ui->error_layout->addWidget(s3_label);

  status_3 = new QLed(ui->centralWidget);
  status_3->setObjectName(QString::fromUtf8("qLed"));
  status_3->setValue(false);
  status_3->setOnColor(QLed::Green);
  status_3->setOffColor(QLed::Red);
  status_3->setShape(QLed::Circle);
  status_3->setSizePolicy(sizePolicy);
  status_3->setMinimumHeight(30);
  status_3->setMinimumWidth(30);

  ui->error_layout->addWidget(status_3);
  */

  status_error_label = new QLabel(ui->centralWidget);
  status_error_label->setObjectName(QString::fromUtf8("label"));
  status_error_label->setContextMenuPolicy(Qt::NoContextMenu);
  status_error_label->setAutoFillBackground(false);
  status_error_label->setAlignment(Qt::AlignCenter);
  status_error_label->setText("SKHI:");

  ui->error_layout->addWidget(status_error_label);

  status_error = new QLed(ui->centralWidget);
  status_error->setObjectName(QString::fromUtf8("qLed"));
  status_error->setValue(false);
  status_error->setOnColor(QLed::Green);
  status_error->setOffColor(QLed::Red);
  status_error->setShape(QLed::Circle);
  status_error->setSizePolicy(sizePolicy);
  status_error->setMinimumHeight(30);
  status_error->setMinimumWidth(30);

  ui->error_layout->addWidget(status_error);

  status_breaking_label = new QLabel(ui->centralWidget);
  status_breaking_label->setObjectName(QString::fromUtf8("label"));
  status_breaking_label->setContextMenuPolicy(Qt::NoContextMenu);
  status_breaking_label->setAutoFillBackground(false);
  status_breaking_label->setAlignment(Qt::AlignCenter);
  status_breaking_label->setText("Break:");

  ui->error_layout->addWidget(status_breaking_label);

  status_breaking = new QLed(ui->centralWidget);
  status_breaking->setObjectName(QString::fromUtf8("qLed"));
  status_breaking->setValue(false);
  status_breaking->setOnColor(QLed::Green);
  status_breaking->setOffColor(QLed::Red);
  status_breaking->setShape(QLed::Circle);
  status_breaking->setSizePolicy(sizePolicy);
  status_breaking->setMinimumHeight(30);
  status_breaking->setMinimumWidth(30);

  ui->error_layout->addWidget(status_breaking);
}

/**
 * @brief 
 * 
 * @param data 
 * @param len 
 */
void MainWindow::SendUdpData(unsigned char * data, uint16_t len)
{
  udp_client->SendUdp(data, len, ui->dest_ip->text());
}

/**
 * @brief 
 * 
 * @param coef 
 */
void MainWindow::SendControllerData(float * coef, uint8_t controller, uint16_t items)
{
  unsigned char *data = new unsigned char[items * sizeof(float) + 4];

  uint16_t index = 0;

  data[index++] = CMD_SET_COEFFICIENTS;
  data[index++] = controller;

  data[index++] = items >> 8;
  data[index++] = items;

  for (int i = 0; i < items; i++)
  {
    udp_client->FloatToByteTable(coef[i], data, &index);
  }

  SendUdpData(data, index);
  delete[] data;
}

/**
 * @brief 
 * 
 * @param i 
 * @param j 
 * @param gpp_select 
 */
void MainWindow::SetupGpPlot(int i, int j, QComboBox *gpp_select)
{

  plot[i] = new QCustomPlot();
  plot[i]->setObjectName(QString("Ib_plot"));
  QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  sizePolicy.setHeightForWidth(plot[i]->sizePolicy().hasHeightForWidth());
  plot[i]->setSizePolicy(sizePolicy);

  graph_grid[j]->addWidget(plot[i], (0x01 & i) + 1, 0, 1, 1);

  controls_grid[i] = new QGridLayout();
  controls_grid[i]->setSpacing(0);
  controls_grid[i]->setObjectName(QString("gridLayout"));

  controls_grid[i]->addWidget(gpp_select, 0, 1, 1, 2);

  QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
  x_scale[i] = new QDial();
  x_scale[i]->setObjectName(QString("x_scale"));
  sizePolicy1.setHorizontalStretch(0);
  sizePolicy1.setVerticalStretch(0);
  sizePolicy1.setHeightForWidth(x_scale[i]->sizePolicy().hasHeightForWidth());
  x_scale[i]->setSizePolicy(sizePolicy1);
  x_scale[i]->setMaximum(10000);

  controls_grid[i]->addWidget(x_scale[i], 3, 1, 1, 1);

  y_scale[i] = new QDial();
  y_scale[i]->setObjectName(QString("y_scale"));
  sizePolicy1.setHeightForWidth(y_scale[i]->sizePolicy().hasHeightForWidth());
  y_scale[i]->setSizePolicy(sizePolicy1);
  y_scale[i]->setMaximum(200);

  controls_grid[i]->addWidget(y_scale[i], 1, 1, 1, 1);

  x_scales_label[i] = new QLabel();
  x_scales_label[i]->setObjectName(QString("x_scales_label"));
  sizePolicy1.setHeightForWidth(x_scales_label[i]->sizePolicy().hasHeightForWidth());
  x_scales_label[i]->setSizePolicy(sizePolicy1);
  x_scales_label[i]->setAlignment(Qt::AlignCenter);
  x_scales_label[i]->setMargin(5);

  controls_grid[i]->addWidget(x_scales_label[i], 3, 0, 1, 1);

  y_scales_label[i] = new QLabel();
  y_scales_label[i]->setObjectName(QString("y_scales_label"));
  sizePolicy1.setHeightForWidth(y_scales_label[i]->sizePolicy().hasHeightForWidth());
  y_scales_label[i]->setSizePolicy(sizePolicy1);
  y_scales_label[i]->setAlignment(Qt::AlignCenter);
  y_scales_label[i]->setMargin(5);

  controls_grid[i]->addWidget(y_scales_label[i], 1, 0, 1, 1);

  y_scale_edit[i] = new QLineEdit();
  y_scale_edit[i]->setObjectName(QString("yscale_edit"));
  QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
  sizePolicy2.setHorizontalStretch(0);
  sizePolicy2.setVerticalStretch(0);
  sizePolicy2.setHeightForWidth(y_scale_edit[i]->sizePolicy().hasHeightForWidth());
  y_scale_edit[i]->setSizePolicy(sizePolicy2);
  y_scale_edit[i]->setMaximumSize(QSize(90, 20));
  QFont font;
  font.setPointSize(8);
  y_scale_edit[i]->setFont(font);
  y_scale_edit[i]->setMaxLength(10);
  y_scale_edit[i]->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);

  controls_grid[i]->addWidget(y_scale_edit[i], 2, 1, 1, 1);

  x_scale_edit[i] = new QLineEdit();
  x_scale_edit[i]->setObjectName(QString("xscale_edit"));
  sizePolicy2.setHeightForWidth(x_scale_edit[i]->sizePolicy().hasHeightForWidth());
  x_scale_edit[i]->setSizePolicy(sizePolicy2);
  x_scale_edit[i]->setMaximumSize(QSize(90, 20));
  x_scale_edit[i]->setFont(font);
  x_scale_edit[i]->setMaxLength(10);
  x_scale_edit[i]->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);

  controls_grid[i]->addWidget(x_scale_edit[i], 4, 1, 1, 1);

  graph_grid[j]->addLayout(controls_grid[i], (0x01 & i) + 1, 1, 1, 1);

  x_scales_label[i]->setText(QApplication::translate("MainWindow", "x scale:", 0));
  y_scales_label[i]->setText(QApplication::translate("MainWindow", "y scale:", 0));
}

#ifndef PLOT_SELECT_H
#define PLOT_SELECT_H

#include <QDialog>
#include <QCheckBox>

#define MAX_PLOTS           10
#define MAX_PATTER_POINTS   6
#define MAX_COEFFICIENTS    100


enum
{
  MODE_OPEN_LOOP = 0,
  MODE_SERVO,
  MODE_VELOCITY,
  MODE_I,
  MODE_U,
  MODE_MAX,
};

enum
{
  PATTERN_GRAPH = 0,

  ID_GRAPH,
  IQ_GRAPH,

  AUX_PATTERN_BUFFER,

  IQS_GRAPH,
  IDS_GRAPH,

  UD_GRAPH,
  UQ_GRAPH,

  VELOCITY_GRAPH,
  ANGLE_GRAPH,

  IA_GRAPH,
  IB_GRAPH,
  IC_GRAPH,

  IAS_GRAPH,
  IBS_GRAPH,

  UA_GRAPH,
  UB_GRAPH,
  UC_GRAPH,

  UDC_GRAPH,

  UPD_GRAPH,
  UPD_DOWN_GRAPH,
  UPD_UP_GRAPH,

  UPQ_GRAPH,
  UPQ_DOWN_GRAPH,
  UPQ_UP_GRAPH,
  GP_GRAPH,

  MAX_GRAPHS,
};

namespace Ui 
{
  class plot_select;
}

class PlotSelectClass : public QDialog
{
    Q_OBJECT

public:
    explicit PlotSelectClass(QWidget * parent = 0);
    ~PlotSelectClass();
    bool active_plot[MAX_PLOTS];

public slots:
    void NewPlotList();

private:
    Ui::plot_select * ui;
    QCheckBox * plot[MAX_GRAPHS];
    unsigned char plot_list[MAX_GRAPHS];

signals:
    void SendPlotList(unsigned char * list, uint16_t len);
};

#endif // PLOT_SELECT_H

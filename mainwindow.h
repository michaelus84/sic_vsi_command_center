
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QGridLayout>
#include <QDial>
#include <QDoubleSpinBox>
#include <QWidget>
#include "qcustomplot.h"
#include "client.h"
#include "qled.h"
#include "plot_select.h"
#include "parameters.h"

#define STATUS_RDY                    1
#define STATUS_DESAT                  2
#define STATUS_ERROR                  4
#define STATUS_BREAKING               8

#define REFRESH_PLOT_INTERVAL         50

#define PATTERN_LEN                   11

#define _NumItems(x)                  (sizeof(x)/sizeof(x[0]))

#define EXTERNAL_LOAD_SCALE           2.5194f
#define EXTERNAL_LOAD_OFFSET          2047

enum
{
  OPEN_LOOP_CONTROL = 0,
  VELOCITY_CCS,
  SERVO_SFC,
  SERVO_CCS,
  VELOCITY_NON_LINEAR,
  VELOCITY_SFC_WITH_LC_FILTER,
  UCQ_WITH_LC_FILTER,
  CURRENT_PI,
  SERVO_SFC_IMC,
  VELOCITY_SFC,
  MAX_CONTROLLERS,
};

enum
{
  EXECUTION_TIME = 0,
  ELECTRIC_ANGLE,
  SSI_T_OBS_OLD,
  LC_P_ISDOBS,
  LC_P_UCDOBS,
  LC_P_ISQOBS,
  LC_P_UCQOBS,
};

enum
{
  TAB_1 = 0,
  TAB_2,
  TAB_3,
  TAB_4,
  TAB_5,
};

typedef enum
{
  FREQUENCY_05KHZ = 0,
  FREQUENCY_10KHZ,
  FREQUENCY_15KHZ,
  FREQUENCY_20KHZ,
  FREQUENCY_22KHZ,
  FREQUENCY_25KHZ,
  FREQUENCY_33KHZ,
  FREQUENCY_40KHZ,
  FREQUENCY_48KHZ,
  MAX_FREQUENCY,
} FrequencyTypedef;

typedef struct
{
  QString const *str;
  int items;
} ControllerSchemeTypedef;

typedef enum
{
  PATTERN_STEP = 0,
  PATTERN_LINEAR,
  PATTERN_S_CURVE,
} RampTypeTypedef;


typedef struct
{
  int ct_len;
  int frequency;
  float pattenr_value[MAX_PATTER_POINTS];
  unsigned int pattern_time[MAX_PATTER_POINTS];
  float load_value[MAX_PATTER_POINTS];
  unsigned int load_time[MAX_PATTER_POINTS];
  int controller;
  int mode;
  float coef[MAX_COEFFICIENTS];
  bool new_file;
} InitDataTypedef;


namespace Ui
{
 class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    UdpClientClass * udp_client;
    void resizeEvent(QResizeEvent * event);

    InitDataTypedef config_data;
    QLed * status_rdy;
    QLed * status_desat;
    QLed * status_3;
    QLed * status_error;
    QLed * status_breaking;

    QLabel * status_rdy_label;
    QLabel * status_desat_labe;
    QLabel * s3_label;
    QLabel * status_error_label;
    QLabel * status_breaking_label;

    QString ct_name;

    QString const *controller;

    QCustomPlot * plot[MAX_PLOTS];
    QGridLayout * controls_grid[MAX_PLOTS];
    QDial * x_scale[MAX_PLOTS];
    QDial * y_scale[MAX_PLOTS];
    QLabel * x_scales_label[MAX_PLOTS];
    QLabel * y_scales_label[MAX_PLOTS];
    QLineEdit * x_scale_edit[MAX_PLOTS];
    QLineEdit * y_scale_edit[MAX_PLOTS];
    QDoubleSpinBox * pattern_value[PATTERN_LEN];
    QDoubleSpinBox * d_pattern_value[PATTERN_LEN];
    QSpinBox * pattern_time[PATTERN_LEN];
    unsigned int total_time;

    QSpinBox * load_value[PATTERN_LEN];
    QSpinBox * load_time[PATTERN_LEN];

    QGridLayout * graph_grid[5];

    PlotSelectClass * plotselect;
    ParametersClass * parameters;

    bool pause_recieve;

    QComboBox *gpp_select;

    void SetupPlot(void);

    void SetupGrid(QWidget * parent, int i);
    void SetupPlot(int i, int j);
    void SetupGpPlot(int i, int j, QComboBox * gpp_select);

    void YScaleChange(int scale, int i);
    void XScaleChange(int scale, int i);

    void XScalEdited(int i);
    void YScaleEdited(int i);

    void SetupQLed(void);


private slots:

    int PrametersOpen(void);
    void ResponseExe(uint8_t cmd, uint16_t len, uint8_t * data);

    void x_scale_change_0(int scale);
    void y_scale_change_0(int scale);

    void x_scale_change_1(int scale);
    void y_scale_change_1(int scale);

    void x_scale_change_2(int scale);
    void y_scale_change_2(int scale);

    void x_scale_change_3(int scale);
    void y_scale_change_3(int scale);

    void x_scale_change_4(int scale);
    void y_scale_change_4(int scale);

    void x_scale_change_5(int scale);
    void y_scale_change_5(int scale);

    void x_scale_change_6(int scale);
    void y_scale_change_6(int scale);

    void x_scale_change_7(int scale);
    void y_scale_change_7(int scale);

    void x_scale_change_8(int scale);
    void y_scale_change_8(int scale);

    void x_scale_change_9(int scale);
    void y_scale_change_9(int scale);

    void x_scale_edited_0(void);
    void y_scale_edited_0(void);

    void x_scale_edited_1(void);
    void y_scale_edited_1(void);

    void x_scale_edited_2(void);
    void y_scale_edited_2(void);

    void x_scale_edited_3(void);
    void y_scale_edited_3(void);

    void x_scale_edited_4(void);
    void y_scale_edited_4(void);

    void x_scale_edited_5(void);
    void y_scale_edited_5(void);

    void x_scale_edited_6(void);
    void y_scale_edited_6(void);

    void x_scale_edited_7(void);
    void y_scale_edited_7(void);

    void x_scale_edited_8(void);
    void y_scale_edited_8(void);

    void x_scale_edited_9(void);
    void y_scale_edited_9(void);

    void StopReceive(void);
    void StartReceive(void);

    void Plot(float* data_in);

    void InverterEnable(bool start);
    void InverterStart(void);
    void NewReferenceValue(int value);
    void NewAuxReferenceValue(int value);
    void NewTextReferenceValue(void);
    void NewAuxTextReferenceValue(void);
    void NewReferencePattern(void);
    void NewPatternSlot(int i);
    void InverterStop(void);
    void SaveData(void);
    void ControllerMode(int index);

    void ClearGraphs(void);
    void Restart(void);

    void SendUdpData(unsigned char * data, uint16_t len);
    void SendControllerData(float * coef, uint8_t controller, uint16_t items);

    void SwitchingFrequency(int index);
    void NewLoadPattern(void);

    void PatternEnable(int index);
    void LoadEnable(int index);
    void RampEnable(int index);

    void GeneralPurposePlot(int index);
    void CoefficientOpen(void);

    void OpenLoopSet(int status);
    void OpenLoopRestore(void);

    void Restore(int index);

    void PwmEnable(int status);

    void PolarValue(int status);

private:
    Ui::MainWindow *ui;
    int xscale[MAX_PLOTS];
    double data_len;
    void AdditionalGraphs();
    QCPGraph *graph_ptr[MAX_GRAPHS];
    ControllerSchemeTypedef controllers_scheme[MAX_CONTROLLERS];
};

#endif // MAINWINDOW_H

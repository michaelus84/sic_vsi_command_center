#-------------------------------------------------
#
# Project created by QtCreator 2016-05-08T15:16:14
#
#-------------------------------------------------

QT       += core gui network svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = SiC_VSI_command_center
TEMPLATE = app


SOURCES += main.cpp \
    client.cpp \
    mainwindow.cpp \
    parameters.cpp \
    plot_select.cpp \
    qcustomplot.cpp \
    qled.cpp

HEADERS  += \
    client.h \
    mainwindow.h \
    parameters.h \
    plot_select.h \
    qcustomplot.h \
    qled.h

FORMS += \
    mainwindow.ui \
    parameters.ui \
    plot_select.ui

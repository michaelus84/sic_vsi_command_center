#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QDialog>

namespace Ui {
class parameters;
}

class ParametersClass : public QDialog
{
    Q_OBJECT

public:
    explicit ParametersClass(QWidget *parent = nullptr);
    ~ParametersClass();

  void SetPolePairs(uint8_t pole_pairs);
  void SetAngleOffset(float offset);

private:
    Ui::parameters *ui;
public slots:
    void NewParameters(void);

signals:
  void SendParameters(unsigned char * param, uint16_t len);
};

#endif // PARAMETERS_H
